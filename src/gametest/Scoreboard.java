package gametest;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Scoreboard {

    private static Scoreboard sb;

    public static Scoreboard instance() {
        if (sb == null) {
            sb= new Scoreboard();
        }
        return sb;
    }

    public  class Integral {

        private String playername;
        private int score;
        private int id;

        private Integral(int id, String player, int score) {
            this.playername = player;
            this.score = score;
            this.id = id;
        }
        public int getScore(){
            return score;
        }
        public String getPlayer(){
            return playername;
        }
        public void addScore(int score) {
            this.score += score;
        }

        public int id() {
            return id;
        }

        @Override
        public String toString() {
            return playername + "  Socre : " + score;
        }

    }
    private ArrayList<Integral> integralpair;

    public Scoreboard() {
        this.integralpair = new ArrayList<>();
    }
    public Integral get(int i ){
        return integralpair.get(i);
    }
    public ArrayList<Integral> getWinner(){
        ArrayList<Integral> tmp =new ArrayList<>();
        tmp.add(integralpair.get(0));
        sort();
        for(int i =1;i<integralpair.size();i++){
            if(integralpair.get(i).score==integralpair.get(0).score){
                tmp.add(integralpair.get(i));
            }
        }
        return tmp;
    }
    public void addScore(int ID, String name, int score) {
        findImtegralPair(ID, name).addScore(score);
    }

    public Integral findImtegralPair(int ID, String name) {
        for (int i = 0; i < integralpair.size(); i++) {
            if (integralpair.get(i).id() == ID) {
                return integralpair.get(i);
            }
        }
        return addIntegral(ID, name,0);
    }
    
    public Integral addIntegral(int ID, String name,int score){
        Integral tmp = new Integral(ID, name, score);
        integralpair.add(tmp);
        return tmp;
    }

    public void sort() {
        integralpair.sort((Integral o1, Integral o2) -> {
            return o2.score - o1.score;
        });
    }

    public void clear() {
        integralpair.clear();
    }

    public void paint(Graphics g) {
        g.setFont(FontLoader.Retro5(60f));
        g.setColor(Color.yellow);
        for (int i = 0; i < ((integralpair.size() > 3) ? 3 : integralpair.size()); i++) {
            g.drawString(integralpair.get(i).toString(), 300, 300 + i * 70);
        }
        g.setColor(Color.black);

    }
}
