/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gametest;

import java.awt.Font;

import java.io.File;

import java.io.FileInputStream;
import java.util.HashMap;

public class FontLoader {

    private static HashMap<String, Font> map = new HashMap<>();

    public static Font loadFont(String fontFileName, float fontSize) {
        String key = fontFileName + fontSize;
        if (map.containsKey(key)) {
            return map.get(key);
        }
        try {

            File file = new File(fontFileName);

            FileInputStream aixing = new FileInputStream(file);

            Font dynamicFont = Font.createFont(Font.TRUETYPE_FONT, aixing);

            Font dynamicFontPt = dynamicFont.deriveFont(fontSize);

            aixing.close();
            
            map.put(key, dynamicFontPt);

            return dynamicFontPt;

        } catch (Exception e) {
            e.printStackTrace();
            return new java.awt.Font("宋体", Font.PLAIN, 14);

        }

    }

    public static java.awt.Font Retro1(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro1.ttf", size);
        return font;
    }

    public static java.awt.Font Retro2(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro2.ttf", size);
        return font;
    }

    public static java.awt.Font Retro3(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro3.ttf", size);
        return font;
    }

    public static java.awt.Font Retro4(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro4.ttf", size);
        return font;
    }

    public static java.awt.Font Retro5(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro5.ttf", size);
        return font;
    }

    public static java.awt.Font Retro6(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro6.ttf", size);
        return font;
    }

    public static java.awt.Font Retro7(float size) {
        String root = System.getProperty("user.dir");
        Font font = FontLoader.loadFont(root + "/src/resources/Font/Retro7.ttf", size);
        return font;
    }
}
