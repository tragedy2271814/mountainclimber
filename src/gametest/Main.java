package gametest;

import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import static gametest.Global.*;
import gametest.camera.MapInformation;

public class Main {
    
    public static void main(String[] args)  {
        
        final JFrame jf = new JFrame();
        jf.setTitle("Hyper Climber");
        jf.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //關聯鍵盤按鍵與指令集
        int[][] command = {
            {KeyEvent.VK_LEFT, KeyEvent.VK_LEFT},
            {KeyEvent.VK_RIGHT, KeyEvent.VK_RIGHT},
            {KeyEvent.VK_UP, KeyEvent.VK_UP},
            {KeyEvent.VK_DOWN, KeyEvent.VK_DOWN},
            {KeyEvent.VK_SPACE, KeyEvent.VK_SPACE},
            {KeyEvent.VK_Q, KeyEvent.VK_Q},
            {KeyEvent.VK_W, KeyEvent.VK_W},
            {KeyEvent.VK_E, KeyEvent.VK_E},
            {KeyEvent.VK_R, KeyEvent.VK_R},
            {KeyEvent.VK_ESCAPE, KeyEvent.VK_ESCAPE}
        };
        GI gi = new GI(); //遊戲的本體(邏輯 + 處理)
        GameKernel gk;
        gk = new GameKernel.Builder(gi, LIMIT_DELTA_TIME, NANOSECOND_PER_UPDATE)
                .initListener(command)
                .enableKeyboardTrack(gi)
                .mouseForceRelease()
                .trackChar()
                .keyCleanMode()
                .enableMouseTrack(gi)
                .gen();
        jf.add(gk);
        jf.setVisible(true);
        gk.run(IS_DEBUG);
    }
    
}

