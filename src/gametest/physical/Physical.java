package gametest.physical;

import static gametest.Global.*;


public class Physical {
    public interface PhysicalOrder{
        public abstract void Force();
        public abstract void Portion();
        public abstract void move();
    }
    public interface Counterforce{
    public abstract Vector CounterForce(double x,double y);
}
    //m=質量
    //f=力
    //目前沒用到
    private Vector v;

    public Physical() {
        v =new Vector(0, 0);
    }
    //如果目前的力超過上限則等於上限
    public void MaxVector(){
        if(v.length()>MAXPOWER){
            v.set(MAXPOWER*v.vx()/v.length(),MAXPOWER*v.vy()/v.length());
        }
    }
//    回傳向動量
    public Vector vector(){
        return v;
    }
    public void stop() {
       v.set(0, 0);
    }
//    停止X軸
    public void stopX() {
       v.set(0, v.vy());
    }
//停止Y軸
    public void stopY() {
      v.set(v.vx(), 0);
    }
//反轉X軸
    public void ReverseX() {
       v.set(v.vx() * -0.5, v.vy());
    }
//反轉Y軸
    public void ReverseY() {
       v.set(v.vx(), v.vy() * -0.5);
    }
//    釋放向動量並乘上摩擦力
//    若力量過小直接歸零
    public Vector Consume() {
        if (Math.abs(v.vx()) < 0.03) {
            v.set(0, v.vy() * FRICTION);
        }
        if (Math.abs(v.vy()) < 0.03) {
            v.set(v.vx() * FRICTION, 0);
        }
       v.set(v.vx() * FRICTION, v.vy() * FRICTION);
        return v;
    }
//對目前的向動量施力
    public void Force (double x,double y){
        v.set(v.vx()+x,v.vy() +y);
    }
//    重力
    public Vector GRAVITY(){
        return new Vector(0,GRAVITY);
    }
//    同繩索彈跳繩
    public Vector circularMotion(double x,double y) {
        double r=length(x,y);
        return new Vector(this.v.length()*x/r
                ,this.v.length()*y/r);
    }
}
