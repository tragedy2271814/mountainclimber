package gametest;

public class Path {

    public static final String PLAYER_MOVE_RIGHT = "/MvIn5y-Move_Right.png";
    public static final String PLAYER_MOVE_LEFT = "/MvIn5y-Move_Left.png";
    public static final String PLAYER_NORMAL_RIGHT = "/MvIn5y-Normal_Right.png";
    public static final String PLAYER_NORMAL_LEFT = "/MvIn5y-Normal_Left.png";
    public static final String PLAYER_UP_RIGHT = "/MvIn5y-Up_Right.png";
    public static final String PLAYER_UP_LEFT = "/MvIn5y-Up_Left.png";
    public static final String PLAYER_DOWN_RIGHT = "/MvIn5y-Down_Right.png";
    public static final String PLAYER_DOWN_LEFT = "/MvIn5y-Down_Left.png";
    public static final String PLAYER_WALK_RIGHT = "/MvIn5y-Walk_Right.png";
    public static final String PLAYER_WALK_LEFT = "/MvIn5y-Walk_Left.png";
    public static final String PLAYER_ROLL_RIGHT = "/MvIn5y-Roll_Right.png";
    public static final String PLAYER_ROLL_LEFT = "/MvIn5y-Roll_Left.png";
    public static final String PLAYER_SHOW = "/MvIn5y-Select.png";
    public static final String RESOURCES_FOLDER = "/resources";

    public static class Resources {

        public static final String MAP_FOLDER = RESOURCES_FOLDER + "/Map";
        public static final String TRANSITIONS_FOLDER = RESOURCES_FOLDER + "/Transitions";
        public static final String SOUND_FOLDER = RESOURCES_FOLDER + "/sounds";
        public static final String BACKGROUND_FOLDER = RESOURCES_FOLDER + "/backgrounds";
        public static final String MATERIAL_FOLDER = RESOURCES_FOLDER + "/material";
        public static final String COVER_FOLDER = RESOURCES_FOLDER + "/Cover";
        public static final String EXPLANATION_FOLDER = RESOURCES_FOLDER + "/explanation";

        public static class Map {

            public static final String SINGLEMODEMAP_FOLDER = MAP_FOLDER + "/SingleModeMap";
            public static final String CONNECTMODEMAP_FOLDER = MAP_FOLDER + "/ConnectModeMap";

            public static class SingleModeMap {

                public static final String GENMAP_BMP = SINGLEMODEMAP_FOLDER + "/genMap.bmp";
                public static final String GENMAP_TXT = SINGLEMODEMAP_FOLDER + "/genMap.txt";
            }

            public static class ConnectModeMap {

                public static final String MAP1_FOLDER = CONNECTMODEMAP_FOLDER + "/Map1";
                public static final String MAP2_FOLDER = CONNECTMODEMAP_FOLDER + "/Map2";
                public static final String MAP3_FOLDER = CONNECTMODEMAP_FOLDER + "/Map3";
                public static final String MAP4_FOLDER = CONNECTMODEMAP_FOLDER + "/Map4";

                public static class Map1 {

                    public static final String GENMAP1_BMP = MAP1_FOLDER + "/genMap.bmp";
                    public static final String GENMAP1_TXT = MAP1_FOLDER + "/genMap.txt";
                }

                public static class Map2 {

                    public static final String GENMAP2_BMP = MAP2_FOLDER + "/genMap.bmp";
                    public static final String GENMAP2_TXT = MAP2_FOLDER + "/genMap.txt";
                }

                public static class Map3 {

                    public static final String GENMAP3_BMP = MAP3_FOLDER + "/genMap.bmp";
                    public static final String GENMAP3_TXT = MAP3_FOLDER + "/genMap.txt";
                }

                public static class Map4 {

                    public static final String GENMAP4_BMP = MAP4_FOLDER + "/genMap.bmp";
                    public static final String GENMAP4_TXT = MAP4_FOLDER + "/genMap.txt";
                }
            }
        }

        public static class Transitions {

            public static final String TRANSITIONS = TRANSITIONS_FOLDER + "/Transitions.png";
            public static final String TRANSITIONS2 = TRANSITIONS_FOLDER + "/Transitions2.png";
            public static final String TRANSITIONS3 = TRANSITIONS_FOLDER + "/Transitions3.png";
        }
        public static class Sounds{
            public static final String BGM = SOUND_FOLDER + "/bgm.wav";
            public static final String ROPE = SOUND_FOLDER + "/rope.wav";
        }
        public static class Material {

            public static final String BLOCK_FOLDER = MATERIAL_FOLDER + "/Block";
            public static final String DECORATIONBLOCK_FOLDER = MATERIAL_FOLDER + "/DecorationBlock";
            public static final String LABEL_FOLDER = MATERIAL_FOLDER + "/Label";
            public static final String NORMAL_BLOCK_FOLDER = MATERIAL_FOLDER + "/Normal_Block";
            public static final String PLAYER1_FOLDER = MATERIAL_FOLDER + "/Player1";
            public static final String PLAYER2_FOLDER = MATERIAL_FOLDER + "/Player2";
            public static final String PLAYER3_FOLDER = MATERIAL_FOLDER + "/Player3";
            public static final String PLAYER4_FOLDER = MATERIAL_FOLDER + "/Player4";
            public static final String BLOCK1 = MATERIAL_FOLDER + "/block1.png";
            public static final String BLOCK3 = MATERIAL_FOLDER + "/block3.png";
            public static final String BOUNCEBLOCK1 = MATERIAL_FOLDER + "/block6.png";
            public static final String BLOCK2 = MATERIAL_FOLDER + "/block7.png";
            public static final String SNOWBLOCK1 = MATERIAL_FOLDER + "/block8.png";
            public static final String ICEBLOCK1 = MATERIAL_FOLDER + "/block14.png";
            public static final String TERMAINALPOINT_START = MATERIAL_FOLDER + "/TerminalPoint2.png";
            public static final String TERMAINALPOINT_END = MATERIAL_FOLDER + "/TerminalPoint1.png";
            public static final String TERMAINALPOINT_END2 = MATERIAL_FOLDER + "/TerminalPoint3.png";

            public static class NormalBlock {

                public static final String NORMAL_BLOCK1_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Top.png";
                public static final String NORMAL_BLOCK1_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Bottom.png";
                public static final String NORMAL_BLOCK1_RIGHT = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Right.png";
                public static final String NORMAL_BLOCK1_LEFT = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Left.png";
                public static final String NORMAL_BLOCK1_RIGHT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Right_Top.png";
                public static final String NORMAL_BLOCK1_LEFT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Left_Top.png";
                public static final String NORMAL_BLOCK1_RIGHT_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Right_Bottom.png";
                public static final String NORMAL_BLOCK1_LEFT_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Left_Bottom.png";
                public static final String NORMAL_BLOCK1_MID = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Mid.png";
                public static final String NORMAL_BLOCK2_RIGHT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block2_Right_Top.png";
                public static final String NORMAL_BLOCK2_LEFT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block2_Left_Top.png";
                public static final String NORMAL_BLOCK2_RIGHT_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block2_Right_Bottom.png";
                public static final String NORMAL_BLOCK2_LEFT_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block2_Left_Bottom.png";
                public static final String NORMAL_BLOCK1_ALL = NORMAL_BLOCK_FOLDER + "/Normal_Block1_All.png";
                public static final String NORMAL_BLOCK1_TOP_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Top_Bottom.png";
                public static final String NORMAL_BLOCK1_RIGHT_LEFT = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Right_Left.png";
                public static final String NORMAL_BLOCK2_ALL = NORMAL_BLOCK_FOLDER + "/Normal_Block2_All.png";
                public static final String NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Right_Bottom_Left_Top.png";
                public static final String NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Left_Bottom_Right_Top.png";
                public static final String NORMAL_BLOCK1_THREE_TOP = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Three_Top.png";
                public static final String NORMAL_BLOCK1_THREE_LEFT = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Three_Left.png";
                public static final String NORMAL_BLOCK1_THREE_RIGHT = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Three_Right.png";
                public static final String NORMAL_BLOCK1_THREE_BOTTOM = NORMAL_BLOCK_FOLDER + "/Normal_Block1_Three_Bottom.png";
            }

            public static class Block {

                public static final String BLOCK1_BOTTOM = BLOCK_FOLDER + "/Block1_Bottom.png";
                public static final String BLOCK1_BOTTOM1 = BLOCK_FOLDER + "/Block1_Bottom1.png";
                public static final String BLOCK1_BOTTOM2 = BLOCK_FOLDER + "/Block1_Bottom2.png";
                public static final String BLOCK1_LEFT = BLOCK_FOLDER + "/Block1_Left.png";
                public static final String BLOCK1_LEFT_BOTTOM = BLOCK_FOLDER + "/Block1_Left_Bottom.png";
                public static final String BLOCK1_LEFT_TOP = BLOCK_FOLDER + "/Block1_Left_Top.png";
                public static final String BLOCK1_MID1 = BLOCK_FOLDER + "/Block1_Mid1.png";
                public static final String BLOCK1_MID2 = BLOCK_FOLDER + "/Block1_Mid2.png";
                public static final String BLOCK1_MID3 = BLOCK_FOLDER + "/Block1_Mid3.png";
                public static final String BLOCK1_RIGHT = BLOCK_FOLDER + "/Block1_Right.png";
                public static final String BLOCK1_RIGHT_BOTTOM = BLOCK_FOLDER + "/Block1_Right_Bottom.png";
                public static final String BLOCK1_RIGHT_TOP = BLOCK_FOLDER + "/Block1_Right_Top.png";
                public static final String BLOCK1_TOP = BLOCK_FOLDER + "/Block1_Top.png";
                public static final String BLOCK2_LEFT_BOTTOM = BLOCK_FOLDER + "/Block2_Left_Bottom.png";
                public static final String BLOCK2_LEFT_TOP = BLOCK_FOLDER + "/Block2_Left_Top.png";
                public static final String BLOCK2_RIGHT_BOTTOM = BLOCK_FOLDER + "/Block2_Right_Bottom.png";
                public static final String BLOCK2_RIGHT_TOP = BLOCK_FOLDER + "/Block2_Right_Top.png";
                public static final String BLOCK4_LEFT = BLOCK_FOLDER + "/Block4_Left.png";
                public static final String BLOCK4_MID = BLOCK_FOLDER + "/Block4_Mid1.png";
                public static final String BLOCK4_RIGHT = BLOCK_FOLDER + "/Block4_Right.png";
            }

            public static class DecorationBlock {

                public static final String BRUSH1 = DECORATIONBLOCK_FOLDER + "/Brush1.png";
                public static final String BRUSH2 = DECORATIONBLOCK_FOLDER + "/Brush2.png";
                public static final String BRUSH3 = DECORATIONBLOCK_FOLDER + "/Brush3.png";
                public static final String BRUSH4 = DECORATIONBLOCK_FOLDER + "/Brush4.png";
                public static final String BRUSH5 = DECORATIONBLOCK_FOLDER + "/Brush5.png";
                public static final String TALL_TREE1 = DECORATIONBLOCK_FOLDER + "/Tall_Tree1.png";
                public static final String TALL_TREE2 = DECORATIONBLOCK_FOLDER + "/Tall_Tree2.png";
                public static final String TALL_TREE3 = DECORATIONBLOCK_FOLDER + "/Tall_Tree3.png";
                public static final String TALL_TREE4 = DECORATIONBLOCK_FOLDER + "/Tall_Tree4.png";
                public static final String NORMAL_TREE1 = DECORATIONBLOCK_FOLDER + "/Normal_Tree1.png";
                public static final String NORMAL_TREE2 = DECORATIONBLOCK_FOLDER + "/Normal_Tree2.png";
                public static final String NORMAL_TREE3 = DECORATIONBLOCK_FOLDER + "/Normal_Tree3.png";
                public static final String NORMAL_TREE4 = DECORATIONBLOCK_FOLDER + "/Normal_Tree4.png";
                public static final String SHORT_TREE1 = DECORATIONBLOCK_FOLDER + "/Short_Tree1.png";
                public static final String HOUSE = DECORATIONBLOCK_FOLDER + "/House.png";

            }

            public static class Label {

                public static final String BUTTON_NORMAL = LABEL_FOLDER + "/Button_Normal.png";
                public static final String BUTTON_HARVER = LABEL_FOLDER + "/Button_Harver.png";
            }
        }

        public static class Cover {

            public static final String COVER = COVER_FOLDER + "/cover.png";
            public static final String COVER1 = COVER_FOLDER + "/cover1.png";
        }

        public static class explanation {

            public static final String Q_NORMAL = EXPLANATION_FOLDER + "/Q_Normal.png";
            public static final String Q_PRESSED = EXPLANATION_FOLDER + "/Q_Pressed.png";
            public static final String SPACE_NORMAL = EXPLANATION_FOLDER + "/Space_Normal.png";
            public static final String SPACE_PRESSED = EXPLANATION_FOLDER + "/Space_Pressed.png";
            public static final String W_NORMAL = EXPLANATION_FOLDER + "/W_Normal.png";
            public static final String W_PRESSED = EXPLANATION_FOLDER + "/W_Pressed.png";
            public static final String MOUSE = EXPLANATION_FOLDER + "/mouse.png";
        }

        public static class Background {

            public static final String MOUNTAIN = BACKGROUND_FOLDER + "/mountain.png";
            public static final String MOUNTAIN2 = BACKGROUND_FOLDER + "/mountain3.png";
            public static final String CONNECTSCENE_BACKGROUND = BACKGROUND_FOLDER + "/ConnectScene_Background.png";
            public static final String SCOREBOARDBACKGROUND = BACKGROUND_FOLDER + "/ConnectModeSettlementSceneBackground.png";
        }
    }
}
