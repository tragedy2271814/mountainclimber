package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import gametest.Global;
import static gametest.Global.COLLISION_ACCURACY;
import static gametest.Global.MAXPOWER;
import static gametest.Global.SCREEN_X;
import static gametest.Global.WINDOW_HEIGHT;
import static gametest.Global.WINDOW_WIDTH;
import gametest.Path;
import gametest.camera.MapInformation;
import gametest.controller.AudioResourceController;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.Player;
import gametest.gameOBJ.Rope;
import gametest.physical.Vector;
import static gametest.scene.Pact.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import network.Client.ClientClass;
import network.Server.Server;

public class ConnectMode extends Scene {

    public enum Map {
        MAP1(Path.Resources.Map.ConnectModeMap.Map1.GENMAP1_BMP, Path.Resources.Map.ConnectModeMap.Map1.GENMAP1_TXT, 3200, 6400, 300, 6000),
        MAP2(Path.Resources.Map.ConnectModeMap.Map2.GENMAP2_BMP, Path.Resources.Map.ConnectModeMap.Map2.GENMAP2_TXT, 3200, 3200, 300, 2900),
        MAP3(Path.Resources.Map.ConnectModeMap.Map3.GENMAP3_BMP, Path.Resources.Map.ConnectModeMap.Map3.GENMAP3_TXT, 1600, 6400, 300, 6000),
        MAP4(Path.Resources.Map.ConnectModeMap.Map4.GENMAP4_BMP, Path.Resources.Map.ConnectModeMap.Map4.GENMAP4_TXT, 1600, 3200, 300, 2900);
        private String pathbmp;
        private String pathtxt;
        private int firstX;
        private int firstY;
        private int mapwidth;
        private int mapheight;

        Map(String pathbmp, String pathtxt, int mapwidth, int mapheight, int firstX, int firstY) {
            this.pathbmp = pathbmp;
            this.pathtxt = pathtxt;
            this.mapwidth = mapwidth;
            this.mapheight = mapheight;
            this.firstX = firstX;
            this.firstY = firstY;
        }

        public String getPathbmp() {
            return pathbmp;
        }

        public String getPathtxt() {
            return pathtxt;
        }

        public int getMapWidth() {
            return mapwidth;
        }

        public int getMapHeight() {
            return mapheight;
        }

        public int getFirstX() {
            return firstX;
        }

        public int getFirstY() {
            return firstY;
        }
    }
    private SceneTool st;
    private Point mousepoint;
    private double mouseX;
    private double mouseY;
    private Map map;
    private ArrayList<Button> buttons;
    private boolean parse;
    private Image stopimg;

    public ConnectMode(ArrayList<Player> players, int mapcode) {
        parse = false;
        switch (mapcode) {
            case 1:
                this.map = Map.MAP1;
                break;
            case 2:
                this.map = Map.MAP2;
                break;
            case 3:
                this.map = Map.MAP3;
                break;
            case 4:
                this.map = Map.MAP4;
                break;
        }
        MapInformation.setMapInfo(0, 0, map.getMapWidth(), map.getMapHeight());
        st = new SceneTool.Builder()
                .Timer()
                .setAnimator(Global.State.MOUNTAIN2, null)
                .setMaploader(map.getPathbmp(), map.getPathtxt())
                .setPlayers(players)
                .setSelf(players.get(0))
                .setCam(WINDOW_WIDTH, WINDOW_HEIGHT)
                .ToBright()
                .gen();
        st.setIsConnect(true);
        st.genMap();
        stopimg=ImageController.instance().tryGetImage(Path.Resources.Transitions.TRANSITIONS3);
        buttons = new ArrayList<Button>();
        buttons.add(new Button(SCREEN_X / 2 - 150, 400, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 500, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Disconnect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Disconnect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(0).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                parse = !parse;
            }
        });
        buttons.get(1).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                ClientClass.getInstance().disConnect();
                Server.instance().close();
                SceneController.instance().change(new MainScene());
            }
        });
    }

    @Override
    public void sceneBegin() {
        st.playersReset();
        for (int i = 0; i < st.getPlayers().size(); i++) {
            double centerX = st.getPlayers().get(i).getX();
            st.getPlayers().get(i).collider()
                    .setCenter(map.firstX + i * 25 * ((i % 2 == 0) ? -1 : 1), map.firstY);
            st.getPlayers().get(i).painter()
                    .setCenter(map.firstX + i * 25 * ((i % 2 == 0) ? -1 : 1), map.firstY);
        }
        st.getCam().translate(st.getPlayers().get(0).painter().centerX()
                - st.getCam().collider().centerX(),
                st.getPlayers().get(0).painter().centerY()
                - st.getCam().collider().centerY());
        AudioResourceController.getInstance().loop(Path.Resources.Sounds.BGM, -1);
    }

    @Override
    public void sceneEnd() {
        st.clear();
        ImageController.instance().clear();
    }

    @Override
    public void paint(Graphics g) {
        st.paint(g);
        if (parse) {
            g.drawImage(stopimg, 0, 0, null);
            for (int i = 0; i < buttons.size(); i++) {
                buttons.get(i).paint(g);
            }
        }
    }

    @Override
    public void update() {
        st.update();
        ClientClass.getInstance().sent(UPDATE, bale(Double.toString(st.getSelf().getX()), Double.toString(st.getSelf().getY()), Integer.toString(st.getSelf().Animator().state().typeCode()), Integer.toString(st.getSelf().Animator().molding().code())));
        for (int i = 0; i < st.getBlock().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).getrope1() != null && st.getPlayers().get(j).getrope1().isCollision(st.getBlock().get(i)) && st.getPlayers().get(j).getrope1().state() == Rope.Stage.FLY) {
                    st.getPlayers().get(j).getrope1().isPull(false);
                }
                if (st.getPlayers().get(j).getrope2() != null && st.getPlayers().get(j).getrope2().isCollision(st.getBlock().get(i)) && st.getPlayers().get(j).getrope2().state() == Rope.Stage.FLY) {
                    st.getPlayers().get(j).getrope2().isPull(false);
                }
            }
        }
        if (st.getSelf().getrope1() != null) {
            if ((st.getSelf().getrope1().collider().centerX() < st.getCam().collider().left()
                    || st.getSelf().getrope1().collider().centerX() > st.getCam().collider().right()
                    || st.getSelf().getrope1().collider().centerY() < st.getCam().collider().top()
                    || st.getSelf().getrope1().collider().centerY() > st.getCam().collider().bottom()) && st.getSelf().getrope1().state() == Rope.Stage.FLY) {
                st.getSelf().cancelRope1();
            }
        }
        if (st.getSelf().getrope2() != null) {
            if ((st.getSelf().getrope2().collider().centerX() < st.getCam().collider().left()
                    || st.getSelf().getrope2().collider().centerX() > st.getCam().collider().right()
                    || st.getSelf().getrope2().collider().centerY() < st.getCam().collider().top()
                    || st.getSelf().getrope2().collider().centerY() > st.getCam().collider().bottom()) && st.getSelf().getrope2().state() == Rope.Stage.FLY) {
                st.getSelf().cancelRope2();
            }
        }
        for (int i = 0; i < st.getIceBlocks().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).getrope1() != null && st.getPlayers().get(j).getrope1().isCollision(st.getIceBlocks().get(i))) {
                    st.getPlayers().get(j).cancelRope1();
                }
                if (st.getPlayers().get(j).getrope2() != null && st.getPlayers().get(j).getrope2().isCollision(st.getIceBlocks().get(i))) {
                    st.getPlayers().get(j).cancelRope2();
                }
            }
        }
        for (int i = 0; i < st.getMoldingBlocks().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).isCollision(st.getMoldingBlocks().get(i))) {
                    st.getPlayers().get(j).Animator().setMolding(st.getMoldingBlocks().get(i).molding());
                }
            }
        }
        for (int i = 0; i < st.getTerminalPoints().size(); i++) {
            if (st.getSelf().isCollision(st.getTerminalPoints().get(i)) && st.getTerminalPoints().get(i).animator().state() == Global.State.TERMAINALPOINT_END2) {
                if (!st.isPlayerInRank(st.getSelf().ID())) {
                    st.isReciprocal(true);
                    int tmp = Global.random(1, 4);
                    ClientClass.getInstance().sent(RECIPROCAL_START, bale(tmp + ""));
                    st.setNextMapCode(tmp);
                    switch (st.getRanks().size()) {
                        case 0:
                            st.getSelf().setScore(3);
                            ClientClass.getInstance().sent(GET_SCORE, bale("3"));
                            break;
                        case 1:
                            st.getSelf().setScore(2);
                            ClientClass.getInstance().sent(GET_SCORE, bale("2"));
                            break;
                        default:
                            st.getSelf().setScore(1);
                            ClientClass.getInstance().sent(GET_SCORE, bale("1"));
                            break;
                    }
                    st.getRanks().add(st.getSelf());
                }
            }
        }
        for (int i = 0; i < st.getPlayers().size(); i++) {
            if (st.getPlayers().get(i).getrope1() != null && st.getPlayers().get(i).getrope1().state() == Rope.Stage.FLY) {
                for (int j = 0; j < st.getPlayers().size(); j++) {
                    if (st.getPlayers().get(i) != st.getPlayers().get(j)) {
                        st.getPlayers().get(i).getrope1().collision(st.getPlayers().get(j));
                    }
                }
            }
            if (st.getPlayers().get(i).getrope2() != null && st.getPlayers().get(i).getrope2().state() == Rope.Stage.FLY) {
                for (int j = 0; j < st.getPlayers().size(); j++) {
                    if (st.getPlayers().get(i) != st.getPlayers().get(j)) {
                        st.getPlayers().get(i).getrope2().collision(st.getPlayers().get(j));
                    }
                }
            }
        }
        Force();
        st.consume();
        Portion();
        move();
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            st.getSelf().mouseTrig(e, state, trigTime);
            if (state == CommandSolver.MouseState.MOVED) {
                this.mousepoint = e.getPoint();
                mouseX = e.getPoint().getX() + st.getCam().getX() - Global.WINDOW_WIDTH / 2;
                mouseY = e.getPoint().getY() + st.getCam().getY() - Global.WINDOW_HEIGHT / 2;
                st.getSelf().setMouseX(mouseX);
                st.getSelf().setMouseY(mouseY);
            }
            if(parse){
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {

                if (commandCode == KeyEvent.VK_Q && mousepoint != null && st.getSelf().getrope1() == null) {
                    ClientClass.getInstance().sent(SHOOT_ROPE1, bale(Double.toString(mouseX), Double.toString(mouseY)));
                }
                if (commandCode == KeyEvent.VK_W && mousepoint != null && st.getSelf().getrope2() == null) {
                    ClientClass.getInstance().sent(SHOOT_ROPE2, bale(Double.toString(mouseX), Double.toString(mouseY)));
                }
                st.getSelf().keyPressed(commandCode, trigTime);
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

                if (commandCode == KeyEvent.VK_Q && mousepoint != null && st.getSelf().getrope1() != null) {
                    ClientClass.getInstance().sent(CANCEL_ROPE1, bale(""));
                }
                if (commandCode == KeyEvent.VK_W && mousepoint != null && st.getSelf().getrope2() != null) {
                    ClientClass.getInstance().sent(CANCEL_ROPE2, bale(""));
                }
                if (commandCode == KeyEvent.VK_R) {
                    st.getSelf().reset();
                    st.getSelf().collider()
                            .setCenter(map.firstX, map.firstY);
                    st.getSelf().painter()
                            .setCenter(map.firstX, map.firstY);
                }
                if (commandCode == KeyEvent.VK_ESCAPE) {
                    parse = !parse;
                }
                st.getSelf().keyReleased(commandCode, trigTime);
            }

            @Override
            public void keyTyped(char c, long trigTime) {
                st.getSelf().keyTyped(c, trigTime);
            }
        };
    }

    public void Force() {
        for (int i = 0; i < st.getPlayers().size(); i++) {
            if (st.getPlayers().get(i).getrope1() != null && st.getPlayers().get(i).getrope1().target() != null) {
                if (st.getPlayers().get(i).getrope1().state() == Rope.Stage.PULL) {
                    ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).getrope1().target().ID()),
                            Double.toString(st.getPlayers().get(i).getrope1().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vx() * -0.4),
                            Double.toString(st.getPlayers().get(i).getrope1().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vy() * -0.4)));
                }
            }
            if (st.getPlayers().get(i).getrope2() != null && st.getPlayers().get(i).getrope2().state() == Rope.Stage.PULL && st.getPlayers().get(i).getrope2().target() != null) {
                ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).getrope2().target().ID()),
                        Double.toString(st.getPlayers().get(i).getrope2().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vx() * -0.4),
                        Double.toString(st.getPlayers().get(i).getrope2().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vy() * -0.4)));
            }
        }
        st.getSelf().Force();

    }

    public void Portion() {
        st.getSelf().Portion();
        for (int j = 1; j <= MAXPOWER * COLLISION_ACCURACY; j++) {
            for (int i = 1; i < st.getPlayers().size(); i++) {
                if (st.getCam().isCollision(st.getPlayers().get(i))) {
                    Vector tmp = st.getSelf().collision(st.getPlayers().get(i), j);
                    if (tmp != null) {
                        ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).ID()), Double.toString(tmp.vx()), Double.toString(tmp.vy())));
                    }
                }

            }
            for (int i = 0; i < st.getBlock().size(); i++) {
                if (st.getCam().isCollision(st.getBlock().get(i))) {
                    st.getSelf().collision(st.getBlock().get(i), j);
                }
            }
        }
    }

    public void move() {
        st.getSelf().move();
    }
}
