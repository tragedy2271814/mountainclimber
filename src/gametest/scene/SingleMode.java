package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import gametest.Global;
import static gametest.Global.*;
import gametest.Path;
import gametest.camera.MapInformation;
import gametest.controller.AudioResourceController;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.*;
import gametest.gameOBJ.Rope.Stage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class SingleMode extends Scene {

    private SceneTool st;
    private boolean parse;
    private Image stopimg;
    private ArrayList<Button> buttons;

    public void sceneBegin() {
        parse = false;
        MapInformation.setMapInfo(0, 0, SINGLEMODE_MAP_WIDTH, SINGLEMODE_MAP_HEIGHT);
        st = new SceneTool.Builder()
                .setAnimator(State.MOUNTAIN2, null)
                .setMaploader("/resources/Map/SingleModeMap/genMap.bmp", "/resources/Map/SingleModeMap/genMap.txt")
                .setSelf(new Player(300, 12400))
                .setCam(WINDOW_WIDTH, WINDOW_HEIGHT)
                .Timer()
                .ToBright()
                .gen();
        st.getPlayers().add(st.getSelf());
        st.genMap();
        st.getSelf().isSelf(true);
        AudioResourceController.getInstance().loop(Path.Resources.Sounds.BGM, -1);
        stopimg=ImageController.instance().tryGetImage(Path.Resources.Transitions.TRANSITIONS3);
        buttons = new ArrayList<Button>();
        buttons.add(new Button(SCREEN_X / 2 - 150, 400, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 500, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("ReStart")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 600, new Style.StyleRect(300, 50, true,new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Back to Menu")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Restart")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(2).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Back to Menu")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(0).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                parse=!parse;
                AudioResourceController.getInstance().play(Path.Resources.Sounds.BGM);
            }
        });
        buttons.get(1).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new SingleMode());
            }
        });
        buttons.get(2).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new MainScene());
            }
        });
    }

    @Override
    public void sceneEnd() {
        ImageController.instance().clear();
        AudioResourceController.getInstance().stop(Path.Resources.Sounds.BGM);
    }

    @Override
    public void paint(Graphics g) {
        st.paint(g);
        if(parse){
            g.drawImage(stopimg, 0,0, null);
            for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
        }
    }

    @Override
    public void update() {
        if (!parse) {
            st.update();
            for (int i = 0; i < st.getMoldingBlocks().size(); i++) {
                if (st.getSelf().isCollision(st.getMoldingBlocks().get(i))) {
                    st.getSelf().Animator().setMolding(st.getMoldingBlocks().get(i).molding());
                }
            }
            for (int i = 0; i < st.getTerminalPoints().size(); i++) {
                if (st.getSelf().isCollision(st.getTerminalPoints().get(i)) && st.getTerminalPoints().get(i).animator().state() == Global.State.TERMAINALPOINT_END2) {
                    SceneController.instance().change(new SettlementScene(st.getTime().toString()));
                }
            }
            for (int i = 0; i < st.getBlock().size(); i++) {
                if (st.getSelf().getrope1() != null && st.getSelf().getrope1().isCollision(st.getBlock().get(i)) && st.getSelf().getrope1().state() == Stage.FLY) {
                    st.getSelf().getrope1().isPull(false);
                }
                if (st.getSelf().getrope2() != null && st.getSelf().getrope2().isCollision(st.getBlock().get(i)) && st.getSelf().getrope2().state() == Stage.FLY) {
                    st.getSelf().getrope2().isPull(false);
                }
            }

            if (st.getSelf().getrope1() != null) {
                if ((st.getSelf().getrope1().collider().centerX() < st.getCam().collider().left()
                        || st.getSelf().getrope1().collider().centerX() > st.getCam().collider().right()
                        || st.getSelf().getrope1().collider().centerY() < st.getCam().collider().top()
                        || st.getSelf().getrope1().collider().centerY() > st.getCam().collider().bottom()) && st.getSelf().getrope1().state() == Stage.FLY) {
                    st.getSelf().cancelRope1();
                }
            }
            if (st.getSelf().getrope2() != null) {
                if ((st.getSelf().getrope2().collider().centerX() < st.getCam().collider().left()
                        || st.getSelf().getrope2().collider().centerX() > st.getCam().collider().right()
                        || st.getSelf().getrope2().collider().centerY() < st.getCam().collider().top()
                        || st.getSelf().getrope2().collider().centerY() > st.getCam().collider().bottom()) && st.getSelf().getrope2().state() == Stage.FLY) {
                    st.getSelf().cancelRope2();
                }
            }
            for (int i = 0; i < st.getIceBlocks().size(); i++) {
                if (st.getSelf().getrope1() != null && st.getSelf().getrope1().isCollision(st.getIceBlocks().get(i))) {
                    st.getSelf().cancelRope1();
                }
                if (st.getSelf().getrope2() != null && st.getSelf().getrope2().isCollision(st.getIceBlocks().get(i))) {
                    st.getSelf().cancelRope2();
                }
            }
            Force();
            Portion();
            move();
        }
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {

        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            if (state == CommandSolver.MouseState.MOVED) {
                st.getSelf().setMouseX(e.getPoint().getX() + st.getCam().getX() - Global.WINDOW_WIDTH / 2);
                st.getSelf().setMouseY(e.getPoint().getY() + st.getCam().getY() - Global.WINDOW_HEIGHT / 2);
            }
            st.getSelf().mouseTrig(e, state, trigTime);
            if(parse){
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
                if (!parse) {
                    st.getSelf().keyPressed(commandCode, trigTime);
                }
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {
                if (!parse) {
                    st.getSelf().keyReleased(commandCode, trigTime);
                }
                if (commandCode == KeyEvent.VK_ESCAPE) {
                    parse = !parse;
                    if (!parse) {
                        AudioResourceController.getInstance().play(Path.Resources.Sounds.BGM);
                    } else {
                        AudioResourceController.getInstance().pause(Path.Resources.Sounds.BGM);
                    }
                }
            }

            @Override
            public void keyTyped(char c, long trigTime) {
                st.getSelf().keyTyped(c, trigTime);
            }
        };
    }

    public void Force() {

        st.getSelf().Force();

    }

    public void Portion() {
        st.getSelf().Portion();
        for (int j = 0; j < MAXPOWER * COLLISION_ACCURACY; j++) {
            for (int i = 0; i < st.getBlock().size(); i++) {
                if (st.getCam().isCollision(st.getBlock().get(i))) {
                    st.getSelf().collision(st.getBlock().get(i), j);
                }
            }
        }
    }

    public void move() {
        st.getSelf().move();
    }
}
