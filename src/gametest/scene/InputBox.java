package gametest.scene;

import gametest.CommandSolver;
import gametest.Delay;
import gametest.GameKernel;
import gametest.gameOBJ.Rect;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

public class InputBox implements GameKernel.GameInterface ,CommandSolver.MouseCommandListener,CommandSolver.KeyListener{
    private int limitchar;
    private String text;
    private Delay flashtime;
    private Rect sign;
    private boolean isPaint;
    private Font font;
    public InputBox(int  x,int  y,int width,int height,int limitchar ,int flashtime, Font font){
        this.font=font;
        this.limitchar=limitchar;
        this.flashtime=new Delay(flashtime);
        this.sign=new Rect(x,y,width,height);
    }

    @Override
    public void paint(Graphics g) {
        if(isPaint){
            g.drawRect((int)sign.left()+(text.length()*font.getSize())+10, (int)sign.bottom()-10,font.getSize(), 3);
        }
    }

    @Override
    public void update() {
        if(flashtime.count()){
            isPaint=!isPaint;
        }
    }

    @Override
    public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
    }

    @Override
    public void keyPressed(int commandCode, long trigTime) {
    }

    @Override
    public void keyReleased(int commandCode, long trigTime) {
    }

    @Override
    public void keyTyped(char c, long trigTime) {
    }
}
