package gametest.scene;

import gametest.Animator;
import gametest.CommandSolver;
import gametest.Delay;
import gametest.Global.State;
import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Graphics;

public class TransitionsScene extends Scene{
    private Delay delay;
    private Animator animator;
    @Override
    public void sceneBegin() {
        delay=new Delay(204);
        delay.play();
        animator=new Animator(State.TRANSITIONS,null);
        ImageController.instance().tryGetImage(Path.Resources.Transitions.TRANSITIONS2);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_MID3);
        ImageController.instance().tryGetImage(Path.Resources.Material.TERMAINALPOINT_END2);
        ImageController.instance().tryGetImage(Path.Resources.Material.TERMAINALPOINT_START);
        ImageController.instance().tryGetImage(Path.Resources.Background.MOUNTAIN2);
        ImageController.instance().tryGetImage(Path.Resources.Material.BOUNCEBLOCK1);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH4);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH5);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.HOUSE);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE4);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.SHORT_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE4);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_ALL);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_MID);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_ALL);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER+Path.PLAYER_WALK_RIGHT);
        
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER+Path.PLAYER_WALK_RIGHT);
        
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER+Path.PLAYER_WALK_RIGHT);
        
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER+Path.PLAYER_WALK_RIGHT);
    }

    @Override
    public void sceneEnd() {
        animator=null;
        delay=null;
    }

    @Override
    public void paint(Graphics g) {
        animator.paint(0, 0, g);
    }

    @Override
    public void update() {
        animator.update();
        if(delay.count()){
            SceneController.instance().change(new SingleMode());
        }
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }
    
}
