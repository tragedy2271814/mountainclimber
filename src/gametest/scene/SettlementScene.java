package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.EditText;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import static gametest.Global.SCREEN_X;
import static gametest.Global.SCREEN_Y;
import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SettlementScene extends Scene {

    private String time;
    private EditText namebox;
    private Button back;
    private Button record;
    private BufferedReader br;
    private ArrayList<String> ranks;
    private Image background;

    public SettlementScene(String time) {
        this.time = time;
        try {
            br = new BufferedReader(new FileReader("record.txt"));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        ranks = new ArrayList<>();
        while (true) {
            String tmp;
            try {
                if ((tmp = br.readLine()) == null) {
                    break;
                }
                ranks.add(tmp);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        background = ImageController.instance().tryGetImage(Path.Resources.Background.SCOREBOARDBACKGROUND);
    }

    @Override
    public void sceneBegin() {

        namebox = new EditText(SCREEN_X / 2 -300, 100, "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        namebox.setEditLimit(5);
        back = new Button(SCREEN_X - 225, SCREEN_Y - 100, new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.gray))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        back.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        record = new Button(namebox.right() + 20, namebox.top(), new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.gray))
                .setText("Record")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        record.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
                .setText("Recoed")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        back.setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new MainScene());
            }
        });
        record.setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                ranks.add(namebox.getEditText() + "," + time);
                namebox = null;
                record = null;
                if (ranks.size() > 1) {
                    ranks.sort((String o1, String o2) -> {
                        String[] tmp1 = o1.split(",");
                        String[] tmp2 = o2.split(",");
                        String[] time1 = tmp1[1].split(":");
                        String[] time2 = tmp2[1].split(":");
                        int time1s = Integer.parseInt(time1[0]) * 600 + Integer.parseInt(time1[1]) * 10 + Integer.parseInt(time1[2]);
                        int time2s = Integer.parseInt(time2[0]) * 600 + Integer.parseInt(time2[1]) * 10 + Integer.parseInt(time2[2]);
                        return time1s - time2s;
                    });
                }

                write(ranks);
            }
        });
    }

    public void write(ArrayList<String> ranks) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("record.txt"));
            for (int i = 0; i < ranks.size(); i++) {
                bw.write(ranks.get(i));
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void sceneEnd() {
        back = null;
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, null);
        for (int i = 0; i < ((ranks.size()>5)?5:ranks.size()); i++) {
            String[] tmp = ranks.get(i).split(",");
            g.setColor(Color.yellow);
            g.setFont(FontLoader.Retro5(60f));
            g.drawString((i+1) +"#   " +tmp[0]+"  Time :   "+ tmp[1], SCREEN_X/2-300 ,SCREEN_Y/2 -200+(i*70));
            g.setColor(Color.black);
        }
        g.setColor(Color.red);
        g.setFont(FontLoader.Retro5(70f));
        g.drawString("Your score:" + time, SCREEN_X / 2 - 200, SCREEN_Y / 2 + 240);
        g.setColor(Color.black);
        if (namebox != null) {
            namebox.paint(g);
        }
        if (record != null) {
            record.paint(g);
        }
        back.paint(g);

    }

    @Override
    public void update() {

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            MouseTriggerImpl.mouseTrig(back, e, state);
            if (namebox != null) {
                MouseTriggerImpl.mouseTrig(namebox, e, state);
            }
            if (record != null) {
                MouseTriggerImpl.mouseTrig(record, e, state);
            }

        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {

            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

            }

            @Override
            public void keyTyped(char c, long trigTime) {
                namebox.keyTyped(c);
            }
        };
    }
}
