package gametest.scene;

import gametest.Animator;
import gametest.CommandSolver;
import gametest.Delay;
import gametest.FontLoader;
import gametest.Global;
import static gametest.Global.COLLISION_ACCURACY;
import static gametest.Global.MAXPOWER;
import static gametest.Global.WINDOW_HEIGHT;
import static gametest.Global.WINDOW_WIDTH;
import gametest.Path;
import gametest.camera.MapInformation;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.Player;
import gametest.gameOBJ.Rope;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class ExplanationMode extends Scene {

    private SceneTool st;
    private Animator animator;
    private Delay delay;
    private Image q_normal;
    private Image q_pressed;
    private Image mouse;
    private Image w_normal;
    private Image w_pressed;
    private Image space_normal;
    private Image space_pressed;
    private boolean ispressedQ;
    private boolean ispressedW;
    private boolean ispressedSpace;

    public ExplanationMode() {
        q_normal = ImageController.instance().tryGetImage(Path.Resources.explanation.Q_NORMAL);
        q_pressed = ImageController.instance().tryGetImage(Path.Resources.explanation.Q_PRESSED);
        mouse = ImageController.instance().tryGetImage(Path.Resources.explanation.MOUSE);
        w_normal = ImageController.instance().tryGetImage(Path.Resources.explanation.W_NORMAL);
        w_pressed = ImageController.instance().tryGetImage(Path.Resources.explanation.W_PRESSED);
        space_normal = ImageController.instance().tryGetImage(Path.Resources.explanation.SPACE_NORMAL);
        space_pressed = ImageController.instance().tryGetImage(Path.Resources.explanation.SPACE_PRESSED);
        ispressedQ = false;
        ispressedW = false;
        ispressedSpace = false;
    }

    @Override
    public void sceneBegin() {
        MapInformation.setMapInfo(0, 0, 1280, 1840);
        st = new SceneTool.Builder()
                .setAnimator(Global.State.MOUNTAIN2, null)
                .setMaploader("/resources/ExplanationScene/genMap.bmp", "/resources/ExplanationScene/genMap.txt")
                .setSelf(new Player(300, 1620))
                .setCam(WINDOW_WIDTH, WINDOW_HEIGHT)
                .ToBright()
                .gen();
        st.genMap();

        System.out.println(st.getCam().collider().left() + "　　" + st.getCam().collider().bottom());
        st.getCam().setObj(st.getSelf());
        st.getSelf().isSelf(true);
        st.getPlayers().add(st.getSelf());
    }

    @Override
    public void sceneEnd() {
        ImageController.instance().clear();
    }

    @Override
    public void paint(Graphics g) {
        st.paint(g);
        g.setFont(FontLoader.Retro5(50f));
        g.setColor(Color.red);
        if (ispressedQ) {
            g.drawImage(q_pressed, 150, 60, null);
        } else {
            g.drawImage(q_normal, 150, 60, null);
        }
        g.drawString("+", 220, 100);
        g.drawImage(mouse, 260, 60, null);
        g.drawString(" = red rope", 300, 100);
        g.setColor(Color.WHITE);
        if (ispressedW) {
            g.drawImage(w_pressed, 150, 130, null);
        } else {
            g.drawImage(w_normal, 150, 130, null);
        }
        g.drawString("+", 220, 170);
        g.drawImage(mouse, 260, 130, null);
        g.drawString(" = white rope", st.getCam().cameraWindowX() + 300, st.getCam().cameraWindowY() + 170);
        g.setColor(Color.blue);
        if (ispressedSpace) {
            g.drawImage(space_pressed, 150, 200, null);
        } else {
            g.drawImage(space_normal, 150, 200, null);
        }
        g.drawString(" = pull!", 350, 240);

        g.setColor(Color.WHITE);
        g.setFont(FontLoader.Retro5(35f));
        g.drawString("Ice Block", 330 - (int) st.getCam().collider().left(), 1400 - (int) st.getCam().collider().top());
        g.drawString("Can't grab!", 320 - (int) st.getCam().collider().left(), 1570 - (int) st.getCam().collider().top());
        g.setColor(Color.black);
        g.drawString("Normal Block", 570 - (int) st.getCam().collider().left(), 1400 - (int) st.getCam().collider().top());
        g.drawString("Try to grab it to move.", 500 - (int) st.getCam().collider().left(), 1570 - (int) st.getCam().collider().top());
        g.setColor(Color.GREEN);
        g.drawString("Bounce Block", 830 - (int) st.getCam().collider().left(), 1400 - (int) st.getCam().collider().top());
        g.drawString("Touch it and bounce away!", 790 - (int) st.getCam().collider().left(), 1570 - (int) st.getCam().collider().top());
        g.setFont(FontLoader.Retro5(55f));
        g.setColor(Color.WHITE);
        g.drawString("Goal", 920 - (int) st.getCam().collider().left(), 380 - (int) st.getCam().collider().top());
        g.setColor(Color.RED);
        g.drawString("Using rope to climb as high as you can!", 200 - (int) st.getCam().collider().left(), 1770 - (int) st.getCam().collider().top());

        if (animator != null) {
            animator.paint((int) st.getCam().cameraWindowX(), (int) st.getCam().cameraWindowY(), g);
        }
    }

    @Override
    public void update() {
        st.update();
        for (int i = 0; i < st.getMoldingBlocks().size(); i++) {
            if (st.getSelf().isCollision(st.getMoldingBlocks().get(i))) {
                st.getSelf().Animator().setMolding(st.getMoldingBlocks().get(i).molding());
            }
        }
        for (int i = 0; i < st.getTerminalPoints().size(); i++) {
            if (st.getSelf().isCollision(st.getTerminalPoints().get(i)) && st.getTerminalPoints().get(i).animator().state() == Global.State.TERMAINALPOINT_END2) {
                SceneController.instance().change(new MainScene());
            }
        }
        for (int i = 0; i < st.getBlock().size(); i++) {
            if (st.getSelf().getrope1() != null && st.getSelf().getrope1().isCollision(st.getBlock().get(i)) && st.getSelf().getrope1().state() == Rope.Stage.FLY) {
                st.getSelf().getrope1().isPull(false);
            }
            if (st.getSelf().getrope2() != null && st.getSelf().getrope2().isCollision(st.getBlock().get(i)) && st.getSelf().getrope2().state() == Rope.Stage.FLY) {
                st.getSelf().getrope2().isPull(false);
            }
        }

        if (st.getSelf().getrope1() != null) {
            if (st.getSelf().getrope1().collider().centerX() < st.getCam().collider().left()
                    || st.getSelf().getrope1().collider().centerX() > st.getCam().collider().right()
                    || st.getSelf().getrope1().collider().centerY() < st.getCam().collider().top()
                    || st.getSelf().getrope1().collider().centerY() > st.getCam().collider().bottom()) {
                st.getSelf().cancelRope1();
            }
        }
        if (st.getSelf().getrope2() != null) {
            if (st.getSelf().getrope2().collider().centerX() < st.getCam().collider().left()
                    || st.getSelf().getrope2().collider().centerX() > st.getCam().collider().right()
                    || st.getSelf().getrope2().collider().centerY() < st.getCam().collider().top()
                    || st.getSelf().getrope2().collider().centerY() > st.getCam().collider().bottom()) {
                st.getSelf().cancelRope2();
            }
        }
        for (int i = 0; i < st.getIceBlocks().size(); i++) {
            if (st.getSelf().getrope1() != null && st.getSelf().getrope1().isCollision(st.getIceBlocks().get(i))) {
                st.getSelf().cancelRope1();
            }
            if (st.getSelf().getrope2() != null && st.getSelf().getrope2().isCollision(st.getIceBlocks().get(i))) {
                st.getSelf().cancelRope2();
            }
        }
        Force();
        Portion();
        move();
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {

        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            if (state == CommandSolver.MouseState.MOVED) {
                st.getSelf().setMouseX(e.getPoint().getX() + st.getCam().getX() - Global.WINDOW_WIDTH / 2);
                st.getSelf().setMouseY(e.getPoint().getY() + st.getCam().getY() - Global.WINDOW_HEIGHT / 2);
            }
            st.getSelf().mouseTrig(e, state, trigTime);
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
                st.getSelf().keyPressed(commandCode, trigTime);
                if (commandCode == KeyEvent.VK_Q) {
                    ispressedQ = true;
                }
                if (commandCode == KeyEvent.VK_W) {
                    ispressedW = true;
                }
                if (commandCode == KeyEvent.VK_SPACE) {
                    ispressedSpace = true;
                }
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {
                st.getSelf().keyReleased(commandCode, trigTime);
                if (commandCode == KeyEvent.VK_Q) {
                    ispressedQ = false;
                }
                if (commandCode == KeyEvent.VK_W) {
                    ispressedW = false;
                }
                if (commandCode == KeyEvent.VK_SPACE) {
                    ispressedSpace = false;
                }
            }

            @Override
            public void keyTyped(char c, long trigTime) {
                st.getSelf().keyTyped(c, trigTime);
            }
        };
    }

    public void Force() {

        st.getSelf().Force();

    }

    public void Portion() {
        st.getSelf().Portion();
        for (int j = 0; j < MAXPOWER*COLLISION_ACCURACY; j++) {
            for (int i = 0; i < st.getBlock().size(); i++) {
                st.getSelf().collision(st.getBlock().get(i), j);
            }
        }
    }

    public void move() {
        st.getSelf().move();
    }
}
