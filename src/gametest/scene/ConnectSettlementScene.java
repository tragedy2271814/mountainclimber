package gametest.scene;

import gametest.CommandSolver;
import gametest.Delay;
import gametest.FontLoader;
import static gametest.Global.SCREEN_X;
import static gametest.Global.SCREEN_Y;
import gametest.Path;
import gametest.Scoreboard;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.Player;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

public class ConnectSettlementScene extends Scene{
    private Delay delay; 
    private ArrayList<Player> players;
    private int nextmapcode;
    private Image background;
    public ConnectSettlementScene(ArrayList<Player> players,int nextmapcode){
        this.delay=new Delay(300);
        delay.play();
        this.players=players;
        this.nextmapcode=nextmapcode;
        background = ImageController.instance().tryGetImage(Path.Resources.Background.SCOREBOARDBACKGROUND);
    }

    @Override
    public void sceneBegin() {
        for(int i=0;i<players.size();i++){
            Scoreboard.instance().addScore(players.get(i).ID(),players.get(i).getName(),players.get(i).getScore());
            
        }
        Scoreboard.instance().sort();
    }

    @Override
    public void sceneEnd() {
        for(int i =0;i<players.size();i++){
            players.get(i).setScore(0);
        }
        delay=null;
        players=null;
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, null);
        Scoreboard.instance().paint(g);
        g.setColor(Color.red);
        g.setFont(FontLoader.Retro5(70));
        g.drawString("Your score : "+ Scoreboard.instance().findImtegralPair(players.get(0).ID(),players.get(0).getName()).getScore(), SCREEN_X / 2 - 200, SCREEN_Y / 2 + 240);
        g.setColor(Color.black);
    }

    @Override
    public void update() {
        if(delay.count()){
            if(Scoreboard.instance().getWinner().get(0).getScore()>=6){
                SceneController.instance().change(new WinnerScene());
            }else{
                SceneController.instance().change(new ConnectMode(players,nextmapcode));
            }
        }
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }
    
}
