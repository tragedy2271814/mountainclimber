package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import static gametest.Global.SCREEN_X;
import static gametest.Global.SCREEN_Y;
import gametest.Path;
import gametest.Scoreboard;
import gametest.Scoreboard.Integral;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import network.Client.ClientClass;
import network.Server.Server;

public class WinnerScene extends Scene{
    private ArrayList<Integral> ranks;
    private Button back;
    private Image background;
    
    public WinnerScene(){
        this.ranks=Scoreboard.instance().getWinner();
        back = new Button(SCREEN_X - 225, SCREEN_Y - 100, new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.gray))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        back.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.black));
        back.setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new MainScene());
            }
        });
        background = ImageController.instance().tryGetImage(Path.Resources.Background.SCOREBOARDBACKGROUND);
    }
    @Override
    public void sceneBegin() {
        
    }

    @Override
    public void sceneEnd() {
        Scoreboard.instance().clear();
        ClientClass.getInstance().disConnect();
        Server.instance().close();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, null);
        g.setColor(Color.yellow);
        g.setFont(FontLoader.Retro5(100f));
        g.drawString("Winner", SCREEN_X/2-100, 100);
        g.setFont(FontLoader.Retro5(60f));
        for(int i =0;i<ranks.size();i++){
            g.drawString(ranks.get(i).toString(),SCREEN_X/2-200, 300+i*70);
        }
        g.setColor(Color.black);
        back.paint(g);
    }

    @Override
    public void update() {
        
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(back, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
            
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {
            
            }

            @Override
            public void keyTyped(char c, long trigTime) {
            
            }
        };
    }
    
}
