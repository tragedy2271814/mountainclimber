package gametest.scene;

import gametest.Animator;
import gametest.Delay;
import gametest.GameKernel;
import gametest.Global.*;
import static gametest.Global.SCREEN_X;
import gametest.Time;
import gametest.camera.Camera;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.BounceBlock;
import gametest.gameOBJ.DecorationBlock;
import gametest.gameOBJ.GameObject;
import gametest.gameOBJ.IceBlock;
import gametest.gameOBJ.MoldingBlock;
import gametest.gameOBJ.NormalBlock;
import gametest.gameOBJ.Player;
import gametest.gameOBJ.TerminalPoint;
import gametest.physical.Vector;
import static gametest.scene.Pact.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import maploader.MapInfo;
import maploader.MapLoader;
import network.Client.ClientClass;
import network.Client.CommandReceiver;
import network.Server.Server;

public class SceneTool implements GameKernel.GameInterface {

    public static class Builder {

        private Animator animator;
        private Image background;
        private Player self;
        private MapLoader maploader;
        private ArrayList<Player> players;
        private ArrayList<MapInfo> mi;
        private ArrayList<GameObject> Block;
        private ArrayList<GameObject> iceBlocks;
        private ArrayList<MoldingBlock> moldingBlocks;
        private ArrayList<TerminalPoint> terminalPoints;
        private ArrayList<DecorationBlock> decorationBlocks;
        private Camera cam;
        private Time time;

        private boolean isTobright;
        private Animator tobright;
        private Delay tobrighttime;

        public Builder() {
            players = new ArrayList<Player>();
            Block = new ArrayList();
            iceBlocks = new ArrayList();
            moldingBlocks = new ArrayList();
            terminalPoints = new ArrayList();
            decorationBlocks = new ArrayList();
            isTobright = false;
        }

        public Builder ToBright() {
            isTobright = true;
            this.tobright = new Animator(State.TOBRIGHT);
            this.tobrighttime = new Delay(State.TOBRIGHT.speed() * State.TOBRIGHT.arr().length);
            tobrighttime.play();
            return this;
        }

        public Builder setAnimator(State state, Molding molding) {
            this.animator = new Animator(state, molding);
            return this;
        }

        public Builder setPlayers(ArrayList<Player> players) {
            this.players = players;
            return this;
        }

        public Builder setBackground(String path) {
            background = ImageController.instance().tryGetImage(path);
            return this;
        }

        public Builder setSelf(Player self) {
            this.self = self;
            return this;
        }

        public Builder setMaploader(String MapPath, String txtPath) {
            try {
                this.maploader = new MapLoader(MapPath, txtPath);
            } catch (IOException ex) {
                Logger.getLogger(SceneTool.class.getName()).log(Level.SEVERE, null, ex);
            }
            mi = maploader.combineInfo();
            return this;
        }

        public Builder setCam(int width, int heigth) {
            this.cam = new Camera.Builder(width, heigth)
                    .setCameraStartLocation(0, 0)
                    .setCameraWindowLocation(0, 0)
                    .setChaseObj(self)
                    .gen();
            cam.setObj(self);
            return this;
        }

        public Builder Timer() {
            time = new Time();
            return this;
        }

        public SceneTool gen() {
            return new SceneTool(animator, background, self, maploader, players, mi, Block, iceBlocks, moldingBlocks, terminalPoints, decorationBlocks, cam, time, isTobright, tobright, tobrighttime);
        }
    }
    private Animator animator;
    private Image background;
    private Player self;
    private MapLoader maploader;
    private ArrayList<Player> players;
    private ArrayList<MapInfo> mi;
    private ArrayList<GameObject> Block;
    private ArrayList<GameObject> iceBlocks;
    private ArrayList<MoldingBlock> moldingBlocks;
    private ArrayList<TerminalPoint> terminalPoints;
    private ArrayList<DecorationBlock> decorationBlocks;
    private Camera cam;
    private Time time;
    private boolean isConnect;
    private boolean isTobright;
    private Animator tobright;
    private Delay tobrighttime;
    private int count;
    private boolean isReciprocal;
    private ArrayList<Player> rank;
    private int nextmapcode;

    private SceneTool(Animator animator, Image background, Player self, MapLoader maploader, ArrayList<Player> players, ArrayList<MapInfo> mi, ArrayList<GameObject> Block,
            ArrayList<GameObject> iceBlocks, ArrayList<MoldingBlock> moldingBlocks, ArrayList<TerminalPoint> terminalPoints, ArrayList<DecorationBlock> decorationBlocks,
            Camera cam, Time time, boolean isTobright, Animator tobright, Delay tobrighttime) {
        this.animator = animator;
        this.background = background;
        this.self = self;
        this.maploader = maploader;
        this.players = players;
        this.mi = mi;
        this.Block = Block;
        this.iceBlocks = iceBlocks;
        this.moldingBlocks = moldingBlocks;
        this.terminalPoints = terminalPoints;
        this.decorationBlocks = decorationBlocks;
        this.cam = cam;
        this.time = time;
        isConnect = false;
        this.isTobright = isTobright;
        this.tobright = tobright;
        this.tobrighttime = tobrighttime;
        isReciprocal = false;
        this.count = 1800;
        rank = new ArrayList<>();

    }

    public ArrayList<Player> getRanks() {
        return rank;
    }

    public void isReciprocal(boolean isReciprocal) {
        this.isReciprocal = isReciprocal;
    }

    public boolean isPlayerInRank(int id) {
        for (int i = 0; i < rank.size(); i++) {
            if (rank.get(i).ID() == id) {
                return true;
            }
        }
        return false;
    }

    public void playersReset() {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).reset();
        }
    }

    public void setSelf(Player self) {
        this.self = self;
    }

    public void setNextMapCode(int nextmapcode) {
        this.nextmapcode = nextmapcode;
    }

    public void setCam(Camera cam) {
        this.cam = cam;
    }

    public void setIsConnect(boolean isConnect) {
        this.isConnect = isConnect;
    }

    public Image getImg() {
        return background;
    }

    public Animator getAnimator() {
        return animator;
    }

    public Player getSelf() {
        return self;
    }

    public MapLoader getMaploader() {
        return maploader;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public ArrayList<MapInfo> getMi() {
        return mi;
    }

    public ArrayList<GameObject> getBlock() {
        return Block;
    }

    public ArrayList<GameObject> getIceBlocks() {
        return iceBlocks;
    }

    public ArrayList<MoldingBlock> getMoldingBlocks() {
        return moldingBlocks;
    }

    public ArrayList<TerminalPoint> getTerminalPoints() {
        return terminalPoints;
    }

    public ArrayList<DecorationBlock> getDecorationBlocks() {
        return decorationBlocks;
    }

    public Camera getCam() {
        return cam;
    }

    public Time getTime() {
        return time;
    }

    public void createRoom(int port) {
        Server.instance().create(port);
        Server.instance().start();
    }

    public String[] serverInformation() {
        return Server.instance().getLocalAddress();
    }

    public void connect(String host, int port) throws IOException {
        ClientClass.getInstance().connect(host, port);
        if (self != null) {
            self.setID(ClientClass.getInstance().getID());
        }
        isConnect = true;
    }

    public void genMap() {
        Block = maploader.creatObjectArray("Block", 64, mi, (String gameObject, String name, MapInfo mapInfo, int MapObjectSize) -> {
            if (name.equals("Normal_Block1_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_BOTTOM);
            }
            if (name.equals("Normal_Block1_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_TOP);
            }
            if (name.equals("Normal_Block1_Right")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_RIGHT);
            }
            if (name.equals("Normal_Block1_Left")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_LEFT);
            }
            if (name.equals("Normal_Block1_Right_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_RIGHT_BOTTOM);
            }
            if (name.equals("Normal_Block1_Left_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_LEFT_TOP);
            }
            if (name.equals("Normal_Block1_Right_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_RIGHT_TOP);
            }
            if (name.equals("Normal_Block1_Left_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_LEFT_BOTTOM);
            }
            if (name.equals("Normal_Block2_Right_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_RIGHT_BOTTOM);
            }
            if (name.equals("Normal_Block2_Left_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_LEFT_TOP);
            }
            if (name.equals("Normal_Block2_Right_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_RIGHT_TOP);
            }
            if (name.equals("Normal_Block2_Left_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_LEFT_BOTTOM);
            }
            if (name.equals("Normal_Block1_Mid")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_MID);
            }
            if (name.equals("Normal_Block_All")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_ALL);
            }
            if (name.equals("Normal_Block_Left_Bottom_Right_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP);
            }
            if (name.equals("Normal_Block_Right_Bottom_Left_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP);
            }
            if (name.equals("Normal_Block1_Top_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_TOP_BOTTOM);
            }
            if (name.equals("Normal_Block1_Right_Left")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_RIGHT_LEFT);
            }
            if (name.equals("Normal_Block2_All")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK2_ALL);
            }
            if (name.equals("Normal_Block1_Three_Top")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_THREE_TOP);
            }
            if (name.equals("Normal_Block1_Three_Right")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_THREE_RIGHT);
            }
            if (name.equals("Normal_Block1_Three_Left")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_THREE_LEFT);
            }
            if (name.equals("Normal_Block1_Three_Bottom")) {
                return new NormalBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, NormalBlock.Type.NORMAL_BLOCK1_THREE_BOTTOM);
            }
            if (name.equals("BounceBlock")) {
                return new BounceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize);
            }
            if (name.equals("Block1_Bottom")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_BOTTOM);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Bottom1")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_BOTTOM1);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Bottom2")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_BOTTOM2);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Left")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_LEFT);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Left_Bottom")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_LEFT_BOTTOM);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Left_Top")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_LEFT_TOP);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Mid1")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_MID1);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Mid2")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_MID2);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Mid3")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_MID3);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Right")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_RIGHT);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Right_Bottom")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_RIGHT_BOTTOM);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Right_Top")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_RIGHT_TOP);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block1_Top")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK1_TOP);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block2_Left_Bottom")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK2_LEFT_BOTTOM);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block2_Left_Top")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK2_LEFT_TOP);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block2_Right_Bottom")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK2_RIGHT_BOTTOM);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Block2_Right_Top")) {
                IceBlock tmp = new IceBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize, mapInfo.getSizeX() * MapObjectSize, mapInfo.getSizeY() * MapObjectSize, IceBlock.Type.BLOCK2_RIGHT_TOP);
                iceBlocks.add(tmp);
                return tmp;
            }
            if (name.equals("Player1")) {
                MoldingBlock tmp = new MoldingBlock(mapInfo.getX() * MapObjectSize + 22, mapInfo.getY() * MapObjectSize + 37, Molding.PLAYER1);
                moldingBlocks.add(tmp);
                return null;
            }
            if (name.equals("Player2")) {
                MoldingBlock tmp = new MoldingBlock(mapInfo.getX() * MapObjectSize + 22, mapInfo.getY() * MapObjectSize + 37, Molding.PLAYER2);
                moldingBlocks.add(tmp);
                return null;
            }
            if (name.equals("Player3")) {
                MoldingBlock tmp = new MoldingBlock(mapInfo.getX() * MapObjectSize + 22, mapInfo.getY() * MapObjectSize + 37, Molding.PLAYER3);
                moldingBlocks.add(tmp);
                return null;
            }
            if (name.equals("Player4")) {
                MoldingBlock tmp = new MoldingBlock(mapInfo.getX() * MapObjectSize + 22, mapInfo.getY() * MapObjectSize + 37, Molding.PLAYER4);
                moldingBlocks.add(tmp);
                return null;
            }
            if (name.equals("TerminalPoint")) {
                TerminalPoint tmp = new TerminalPoint(mapInfo.getX() * MapObjectSize - 17, mapInfo.getY() * MapObjectSize - State.TERMAINALPOINT_END2.height() + MapObjectSize, State.TERMAINALPOINT_END2);
                terminalPoints.add(tmp);
                return null;
            }
            if (name.equals("TerminalPoint2")) {
                TerminalPoint tmp = new TerminalPoint(mapInfo.getX() * MapObjectSize - 17, mapInfo.getY() * MapObjectSize + 32, State.TERMAINALPOINT_START);
                terminalPoints.add(tmp);
                return null;
            }

            if (name.equals("Brush1")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.BRUSH1.height() + MapObjectSize, DecorationBlock.Type.BRUSH1);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Brush2")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.BRUSH2.height() + MapObjectSize, DecorationBlock.Type.BRUSH2);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Brush3")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.BRUSH3.height() + MapObjectSize, DecorationBlock.Type.BRUSH3);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Brush4")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.BRUSH4.height() + MapObjectSize, DecorationBlock.Type.BRUSH4);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Brush5")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.BRUSH5.height() + MapObjectSize, DecorationBlock.Type.BRUSH5);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Normal_Tree1")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.NORMAL_TREE1.height() + MapObjectSize, DecorationBlock.Type.NORMAL_TREE1);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Normal_Tree2")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.NORMAL_TREE2.height() + MapObjectSize, DecorationBlock.Type.NORMAL_TREE2);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Normal_Tree3")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.NORMAL_TREE3.height() + MapObjectSize, DecorationBlock.Type.NORMAL_TREE3);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Normal_Tree4")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.NORMAL_TREE4.height() + MapObjectSize, DecorationBlock.Type.NORMAL_TREE4);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Short_Tree1")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.SHORT_TREE1.height() + MapObjectSize, DecorationBlock.Type.SHORT_TREE1);
                decorationBlocks.add(tmp);
                return null;
            }
            if (name.equals("Tall_Tree1")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.TALL_TREE1.height() + MapObjectSize, DecorationBlock.Type.TALL_TREE1);
                decorationBlocks.add(tmp);
                return null;
            }
//            if (name.equals("Tall_Tree2")) {
//                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.TALL_TREE2.height() + MapObjectSize, DecorationBlock.Type.TALL_TREE2);
//                decorationBlocks.add(tmp);
//                return null;
//            }
            if (name.equals("Tall_Tree3")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.TALL_TREE3.height() + MapObjectSize, DecorationBlock.Type.TALL_TREE3);
                decorationBlocks.add(tmp);
                return null;
            }
//            if (name.equals("Tall_Tree4")) {
//                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.TALL_TREE4.height() + MapObjectSize, DecorationBlock.Type.TALL_TREE4);
//                decorationBlocks.add(tmp);
//                return null;
//            }
            if (name.equals("House")) {
                DecorationBlock tmp = new DecorationBlock(mapInfo.getX() * MapObjectSize, mapInfo.getY() * MapObjectSize - DecorationBlock.Type.HOUSE.height() + MapObjectSize, DecorationBlock.Type.HOUSE);
                decorationBlocks.add(tmp);
                return null;
            }
            return null;
        });
    }

    public void clear() {
        animator = null;
        background = null;
        self = null;
        maploader = null;
        players = null;
        mi = null;
        Block = null;
        iceBlocks = null;
        moldingBlocks = null;
        terminalPoints = null;
        decorationBlocks = null;
        cam = null;
        time = null;
    }

    @Override
    public void paint(Graphics g) {
        if (cam != null) {
            cam.start(g);
            if (background != null) {
                g.drawImage(background, (int) cam.painter().left(), (int) cam.painter().top(), null);
            }
            if (animator != null) {
                animator.paint((int) cam.painter().left(), (int) cam.painter().top(), g);
            }

            for (int i = 0; i < decorationBlocks.size(); i++) {
                if (cam.isCollision(decorationBlocks.get(i))) {
                    decorationBlocks.get(i).paint(g);
                }
            }
            for (int i = 0; i < Block.size(); i++) {
                if (cam.isCollision(Block.get(i))) {
                    Block.get(i).paint(g);
                }
            }
            for (int i = 0; i < moldingBlocks.size(); i++) {
                if (cam.isCollision(moldingBlocks.get(i))) {
                    moldingBlocks.get(i).paint(g);
                }
            }
            for (int i = 0; i < terminalPoints.size(); i++) {
                if (cam.isCollision(terminalPoints.get(i))) {
                    terminalPoints.get(i).paint(g);
                }
            }
            for (int i = 0; i < players.size(); i++) {
                players.get(i).paint(g);
            }
            if (time != null) {
                time.paint(g);
            }
            if (isReciprocal) {
                g.setColor(Color.red);
                g.drawString(((count / 60 >= 10) ? count / 60 : "0" + count / 60) + " : " + (count / 6) % 10, (int) cam.collider().left() + SCREEN_X / 2 - 2, (int) cam.collider().top() + 50);
                g.setColor(Color.black);
            }
            cam.end(g);
        } else {
            if (background != null) {
                g.drawImage(background, (int) cam.painter().left(), (int) cam.painter().top(), null);
            }
            if (animator != null) {
                animator.paint(0, 0, g);
            }

            for (int i = 0; i < decorationBlocks.size(); i++) {
                decorationBlocks.get(i).paint(g);
            }
            for (int i = 0; i < Block.size(); i++) {
                Block.get(i).paint(g);
            }
            for (int i = 0; i < moldingBlocks.size(); i++) {
                moldingBlocks.get(i).paint(g);
            }
            for (int i = 0; i < terminalPoints.size(); i++) {
                terminalPoints.get(i).paint(g);
            }
            for (int i = 0; i < players.size(); i++) {
                players.get(i).paint(g);
            }
            if (time != null) {
                time.paint(g);
            }
            if (isReciprocal) {
                g.setColor(Color.red);
                g.drawString(((count / 60 >= 10) ? count / 60 : "0" + count / 60) + " : " + (count / 0.6) % 10, SCREEN_X / 2 - 2, 50);
                g.setColor(Color.black);
            }
        }
        if (isTobright) {
            tobright.paint(0, 0, g);
        }

    }

    @Override
    public void update() {
        if (isTobright) {
            tobright.update();
            if (tobrighttime.count()) {
                isTobright = false;
            }
        }
        if (time != null) {
            if (cam != null) {
                time.setPoint((int) (cam.collider().left() + cam.collider().width() - 200), (int) cam.collider().top() + 100);
            }
            time.update();
        }
        if (cam != null) {
            cam.update();
        }
        if (animator != null) {
            animator.update();
        }
        if (self != null) {
            self.update();
        }
        for (int i = 1; i < players.size(); i++) {
            players.get(i).update();
        }
//        for (int i = 0; i < Block.size(); i++) {
//            Block.get(i).update();
//        }
//        for (int i = 0; i < iceBlocks.size(); i++) {
//            iceBlocks.get(i).update();
//        }
        for (int i = 0; i < moldingBlocks.size(); i++) {
            moldingBlocks.get(i).update();
        }
//        for (int i = 0; i < decorationBlocks.size(); i++) {
//            decorationBlocks.get(i).update();
//        }
//        for (int i = 0; i < terminalPoints.size(); i++) {
//            terminalPoints.get(i).update();
//        }
        if (isReciprocal) {
            if (count == 0) {
                SceneController.instance().change(new ConnectSettlementScene(players, nextmapcode));
            }
            count--;
        }
    }

    public void consume() {
        if (isConnect) {
            ClientClass.getInstance().consume(new CommandReceiver() {
                @Override
                public void receive(int serialNum, int commandCode, ArrayList<String> strs) {
                    switch (commandCode) {
                        case DISCONNECT:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.remove(i);
                                    break;
                                }
                            }
                            break;

                        case CONNECT:
                            boolean isburn = false;
                            for (int i = 0; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    isburn = true;
                                    break;
                                }
                            }
                            if (!isburn) {
                                players.add(new Player((int) Double.parseDouble(strs.get(1)), (int) Double.parseDouble(strs.get(2)), strs.get(0), serialNum));
                                ClientClass.getInstance().sent(Pact.CONNECT, bale(self.getName(), Double.toString(self.getX()), Double.toString(self.getY())));
                            }
                            break;
                        case UPDATE:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).collider().set(Double.parseDouble(strs.get(0)) - players.get(i).collider().width() / 2, Double.parseDouble(strs.get(1)) - players.get(i).collider().height() / 2);
                                    players.get(i).painter().set(Double.parseDouble(strs.get(0)) - players.get(i).collider().width() / 2, Double.parseDouble(strs.get(1)) - players.get(i).collider().height() / 2);
                                    switch (Integer.parseInt(strs.get(2))) {
                                        case 0:
                                            players.get(i).Animator().setState(State.PLAYER_NORMAL_RIGHT);
                                            break;
                                        case 1:
                                            players.get(i).Animator().setState(State.PLAYER_NORMAL_LEFT);
                                            break;
                                        case 2:
                                            players.get(i).Animator().setState(State.PLAYER_MOVE_RIGHT);
                                            break;
                                        case 3:
                                            players.get(i).Animator().setState(State.PLAYER_MOVE_LEFT);
                                            break;
                                        case 4:
                                            players.get(i).Animator().setState(State.PLAYER_UP_RIGHT);
                                            break;
                                        case 5:
                                            players.get(i).Animator().setState(State.PLAYER_UP_LEFT);
                                            break;
                                        case 6:
                                            players.get(i).Animator().setState(State.PLAYER_DOWN_RIGHT);
                                            break;
                                        case 7:
                                            players.get(i).Animator().setState(State.PLAYER_DOWN_LEFT);
                                            break;
                                        case 8:
                                            players.get(i).Animator().setState(State.PLAYER_WALK_RIGHT);
                                            break;
                                        case 9:
                                            players.get(i).Animator().setState(State.PLAYER_WALK_LEFT);
                                            break;
                                        case 10:
                                            players.get(i).Animator().setState(State.PLAYER_ROLL_RIGHT);
                                            break;
                                        case 11:
                                            players.get(i).Animator().setState(State.PLAYER_ROLL_LEFT);
                                            break;
                                    }
                                    switch (Integer.parseInt(strs.get(3))) {
                                        case 0:
                                            players.get(i).Animator().setMolding(Molding.PLAYER1);
                                            break;
                                        case 1:
                                            players.get(i).Animator().setMolding(Molding.PLAYER2);
                                            break;
                                        case 2:
                                            players.get(i).Animator().setMolding(Molding.PLAYER3);
                                            break;
                                        case 3:
                                            players.get(i).Animator().setMolding(Molding.PLAYER4);
                                            break;
                                    }
                                    break;
                                }
                            }
                            break;
                        case SHOOT_ROPE1:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).shootRope1(Double.parseDouble(strs.get(0)), Double.parseDouble(strs.get(1)));
                                }
                            }
                            break;
                        case SHOOT_ROPE2:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).shootRope2(Double.parseDouble(strs.get(0)), Double.parseDouble(strs.get(1)));
                                }
                            }
                            break;
                        case CANCEL_ROPE1:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).cancelRope1();
                                }
                            }
                            break;
                        case CANCEL_ROPE2:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).cancelRope2();
                                }
                            }
                            break;
                        case FORCE:
                            for (int i = 1; i < players.size(); i++) {
                                if (self.ID() == Integer.parseInt(strs.get(0))) {
                                    self.Force(new Vector(Double.parseDouble(strs.get(1)), Double.parseDouble(strs.get(2))));
                                }
                            }
                            break;
                        case START_GAME:
                            SceneController.instance().change(new ConnectMode(players, Integer.parseInt(strs.get(0))));
                            break;
                        case RECIPROCAL_START:
                            isReciprocal = true;
                            nextmapcode = Integer.parseInt(strs.get(0));
                            break;
                        case GET_SCORE:
                            for (int i = 1; i < players.size(); i++) {
                                if (players.get(i).ID() == serialNum) {
                                    players.get(i).setScore(Integer.parseInt(strs.get(0)));
                                    rank.add(players.get(i));
                                }
                            }
                            break;
                    }
                }
            });
        }
    }
}
