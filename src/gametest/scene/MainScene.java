package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.BackgroundType.BackgroundImage;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Label.ClickedAction;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.Animator;
import gametest.CommandSolver;
import gametest.Delay;
import gametest.FontLoader;
import gametest.Global;
import static gametest.Global.*;
import gametest.Path;
import gametest.controller.AudioResourceController;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class MainScene extends Scene {

    private int count;
    private Delay delay;
    private Label label;
    private ArrayList<Button> buttons;
    private Animator animator;

    @Override
    public void sceneBegin() {
        delay = new Delay(4);
        delay.loop();
        count = 0;
        animator=new Animator(State.COVER1,null);
        buttons = new ArrayList<Button>();
        buttons.add(new Button(SCREEN_X / 2 - 150, 400, new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("SingleMode")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 500, new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("CreateRoom")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 600, new Style.StyleRect(300, 50, true,new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("ConnectRoom")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 700, new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Explanation")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("SingleMode")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("CreateRoom")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(2).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("ConnectRoom")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(3).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Explanation")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(0).setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new TransitionsScene());
            }
        });
        buttons.get(1).setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new CreateRoomScene());
            }
        });
        buttons.get(2).setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new ConnectScene());
            }
        });
        buttons.get(3).setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new ExplanationMode());
            }
        });
        AudioResourceController.getInstance().loop(Path.Resources.Sounds.BGM,-1);
    }

    @Override
    public void sceneEnd() {
        buttons = null;
        AudioResourceController.getInstance().stop(Path.Resources.Sounds.BGM);
    }

    @Override
    public void paint(Graphics g) {
        animator.paint(0, 0, g);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
        g.setFont(FontLoader.Retro5(200f));
        g.drawString("Hyper Climber", 180, 300);
    }

    @Override
    public void update() {
        animator.update();

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }

}
