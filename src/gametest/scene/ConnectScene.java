package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.BackgroundType.BackgroundColor;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.EditText;
import ModePackage.menumodule.menu.Label.ClickedAction;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.Style.StyleRect;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import static gametest.Global.*;
import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.Client.ClientClass;

public class ConnectScene extends Scene {

    private Image img;
    private EditText inputip;
    private EditText inputport;
    private EditText inputname;
    private Button connect;
    private Button back;
    private boolean isConnect;

    @Override
    public void sceneBegin() {
        img=ImageController.instance().tryGetImage(Path.Resources.Background.CONNECTSCENE_BACKGROUND);
        isConnect=true;
        inputname = new EditText(SCREEN_X / 2 - 150, 300, "Name", new StyleRect(300, 50, true, new BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        inputip = new EditText(SCREEN_X / 2 - 150, 450, "IP", new StyleRect(300, 50, true, new BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        inputport = new EditText(SCREEN_X / 2 - 150, 600, "Port", new StyleRect(300, 50, true, new BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        connect = new Button(SCREEN_X / 2 +25, 700, new StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Connect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        connect.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Connect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        connect.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                try {
                    ClientClass.getInstance().connect(inputip.getEditText(), Integer.parseInt((inputport.getEditText().equals(""))?"0":inputport.getEditText()));
                    SceneController.instance().change(new WaitingScene( inputname.getEditText()));
                } catch (IOException ex) {
                    ClientClass.getInstance().disConnect();
                    isConnect=false;
                }
                
            }
        });
        back = new Button(SCREEN_X / 2 - 175, 700, new StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        back.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        back.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                
                SceneController.instance().change(new MainScene());
            }
        });
    }

    @Override
    public void sceneEnd() {
        inputip = null;
        inputport = null;
        ImageController.instance().clear();
    }

    @Override
    public void paint(Graphics g) {
        
        g.drawImage(img, 0, 0, null);
        g.setColor(Color.white);
        g.setFont(FontLoader.Retro5(50f));
        g.drawString("Pleace enter your Name:", SCREEN_X / 2 - 150, 280);
        g.drawString("Pleace enter IP:", SCREEN_X / 2 - 150, 430);
        g.drawString("Pleace enter PORT:", SCREEN_X / 2 - 150, 580);
        g.setColor(Color.black);
        if (!isConnect) {
            g.setColor(Color.red);
            g.setFont(FontLoader.Retro5(50f));
            g.drawString("Connect fail!!", SCREEN_X / 2 - 110, 850);
            g.setColor(Color.black);
        }
        inputname.paint(g);
        inputip.paint(g);
        inputport.paint(g);
        connect.paint(g);
        back.paint(g);
    }

    @Override
    public void update() {
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(connect, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
                MouseTriggerImpl.mouseTrig(inputname, e, state);
                MouseTriggerImpl.mouseTrig(inputip, e, state);
                MouseTriggerImpl.mouseTrig(inputport, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {

            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

            }

            @Override
            public void keyTyped(char c, long trigTime) {
                inputip.keyTyped(c);
                inputport.keyTyped(c);
                inputname.keyTyped(c);
            }
        };
    }

}
