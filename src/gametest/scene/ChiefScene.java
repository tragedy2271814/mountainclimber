package gametest.scene;

import ModePackage.menumodule.menu.*;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.Label;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import gametest.Global;
import static gametest.Global.*;
import gametest.Path;
import gametest.camera.MapInformation;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.gameOBJ.Player;
import gametest.gameOBJ.Rope;
import gametest.physical.Vector;
import static gametest.scene.Pact.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.Client.ClientClass;
import network.Server.Server;

public class ChiefScene extends Scene {

    private SceneTool st;
    private Point mousepoint;
    private Button start;
    private ArrayList<Button> buttons;
    private boolean parse;
    private Image stopimg;

    public ChiefScene(String name, int port) {
        MapInformation.setMapInfo(0, 0, WAITINGSCENE_MAP_WIDTH, WAITINGSCENE_MAP_HEIGHT);
        st = new SceneTool.Builder()
                .setSelf(new Player(SCREEN_X / 2, SCREEN_Y / 2, name, 0, true))
                .setMaploader("/resources/WaitingScene/genMap.bmp", "/resources/WaitingScene/genMap.txt")
                .setAnimator(State.MOUNTAIN2, null)
                .ToBright()
                .gen();
        st.createRoom(port);
        try {
            st.connect("127.0.0.1", port);
        } catch (IOException ex) {
            Logger.getLogger(WaitingScene.class.getName()).log(Level.SEVERE, null, ex);
        }
        st.getSelf().setID(ClientClass.getInstance().getID());
        st.getPlayers().add(st.getSelf());
    }

    @Override
    public void sceneBegin() {
        st.genMap();
        start = new Button(SCREEN_X - 225, SCREEN_Y - 100, new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Start")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        start.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Start")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        start.setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                int tmp = Global.random(1, 4);
                ClientClass.getInstance().sent(START_GAME, bale(tmp + ""));
                SceneController.instance().change(new ConnectMode(st.getPlayers(), tmp));
            }
        });
        ImageController.instance().tryGetImage(Path.Resources.Transitions.TRANSITIONS2);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_MID3);
        ImageController.instance().tryGetImage(Path.Resources.Material.TERMAINALPOINT_END2);
        ImageController.instance().tryGetImage(Path.Resources.Material.TERMAINALPOINT_START);
        ImageController.instance().tryGetImage(Path.Resources.Background.MOUNTAIN2);
        ImageController.instance().tryGetImage(Path.Resources.Material.BOUNCEBLOCK1);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK1_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.Block.BLOCK2_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH4);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.BRUSH5);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.HOUSE);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.NORMAL_TREE4);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.SHORT_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE1);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE2);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE3);
        ImageController.instance().tryGetImage(Path.Resources.Material.DecorationBlock.TALL_TREE4);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_ALL);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_MID);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_ALL);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_BOTTOM);
        ImageController.instance().tryGetImage(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_TOP);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER1_FOLDER + Path.PLAYER_WALK_RIGHT);

        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER2_FOLDER + Path.PLAYER_WALK_RIGHT);

        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER3_FOLDER + Path.PLAYER_WALK_RIGHT);

        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_DOWN_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_DOWN_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_MOVE_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_MOVE_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_NORMAL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_NORMAL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_ROLL_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_ROLL_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_UP_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_UP_RIGHT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_WALK_LEFT);
        ImageController.instance().tryGetImage(Path.Resources.Material.PLAYER4_FOLDER + Path.PLAYER_WALK_RIGHT);
        stopimg = ImageController.instance().tryGetImage(Path.Resources.Transitions.TRANSITIONS3);
        buttons = new ArrayList<Button>();
        buttons.add(new Button(SCREEN_X / 2 - 150, 400, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 500, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Disconnect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white)));
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Continue")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Disconnect")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        buttons.get(0).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                parse = !parse;
            }
        });
        buttons.get(1).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                ClientClass.getInstance().disConnect();
                Server.instance().close();
                SceneController.instance().change(new MainScene());
            }
        });

    }

    @Override
    public void sceneEnd() {
        st.clear();
        mousepoint = null;
        start = null;
    }

    @Override
    public void paint(Graphics g) {
        st.paint(g);
        g.setColor(Color.yellow);
        g.drawString(st.serverInformation()[0], 10, 10);
        g.drawString(st.serverInformation()[1], 10, 40);
        g.setColor(Color.black);
        start.paint(g);
        if (parse) {
            g.drawImage(stopimg, 0, 0, null);
            for (int i = 0; i < buttons.size(); i++) {
                buttons.get(i).paint(g);
            }
        }
    }

    @Override
    public void update() {
        st.update();
        ClientClass.getInstance().sent(UPDATE, bale(Double.toString(st.getSelf().getX()), Double.toString(st.getSelf().getY()), Integer.toString(st.getSelf().Animator().state().typeCode()), Integer.toString(st.getSelf().Animator().molding().code())));
        for (int i = 0; i < st.getBlock().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).getrope1() != null && st.getPlayers().get(j).getrope1().isCollision(st.getBlock().get(i)) && st.getPlayers().get(j).getrope1().state() == Rope.Stage.FLY) {
                    st.getPlayers().get(j).getrope1().isPull(false);
                }
                if (st.getPlayers().get(j).getrope2() != null && st.getPlayers().get(j).getrope2().isCollision(st.getBlock().get(i)) && st.getPlayers().get(j).getrope2().state() == Rope.Stage.FLY) {
                    st.getPlayers().get(j).getrope2().isPull(false);
                }
            }
        }
        for (int i = 0; i < st.getIceBlocks().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).getrope1() != null && st.getPlayers().get(j).getrope1().isCollision(st.getIceBlocks().get(i))) {
                    st.getPlayers().get(j).cancelRope1();
                }
                if (st.getPlayers().get(j).getrope2() != null && st.getPlayers().get(j).getrope2().isCollision(st.getIceBlocks().get(i))) {
                    st.getPlayers().get(j).cancelRope2();
                }
            }
        }
        for (int i = 0; i < st.getMoldingBlocks().size(); i++) {
            for (int j = 0; j < st.getPlayers().size(); j++) {
                if (st.getPlayers().get(j).isCollision(st.getMoldingBlocks().get(i))) {
                    st.getPlayers().get(j).Animator().setMolding(st.getMoldingBlocks().get(i).molding());
                }
            }
        }
        for (int i = 0; i < st.getTerminalPoints().size(); i++) {
            if (st.getSelf().isCollision(st.getTerminalPoints().get(i)) && st.getTerminalPoints().get(i).animator().state() == State.TERMAINALPOINT_END) {
//                    換場景
            }
        }
        for (int i = 0; i < st.getPlayers().size(); i++) {
            if (st.getPlayers().get(i).getrope1() != null && st.getPlayers().get(i).getrope1().state() == Rope.Stage.FLY) {
                for (int j = 0; j < st.getPlayers().size(); j++) {
                    if (st.getPlayers().get(i) != st.getPlayers().get(j)) {
                        st.getPlayers().get(i).getrope1().collision(st.getPlayers().get(j));
                    }
                }
            }
            if (st.getPlayers().get(i).getrope2() != null && st.getPlayers().get(i).getrope2().state() == Rope.Stage.FLY) {
                for (int j = 0; j < st.getPlayers().size(); j++) {
                    if (st.getPlayers().get(i) != st.getPlayers().get(j)) {
                        st.getPlayers().get(i).getrope2().collision(st.getPlayers().get(j));
                    }
                }
            }
        }
        Force();
        st.consume();
        Portion();
        st.consume();
        move();
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {

        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            MouseTriggerImpl.mouseTrig(start, e, state);
            st.getSelf().mouseTrig(e, state, trigTime);
            if (state == CommandSolver.MouseState.MOVED) {
                this.mousepoint = e.getPoint();
                st.getSelf().setMouseX(e.getPoint().getX());
                st.getSelf().setMouseY(e.getPoint().getY());
            }
            if (parse) {
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {

                if (commandCode == KeyEvent.VK_Q && mousepoint != null && st.getSelf().getrope1() == null) {
                    ClientClass.getInstance().sent(SHOOT_ROPE1, bale(Double.toString(mousepoint.getX()), Double.toString(mousepoint.getY())));
                }
                if (commandCode == KeyEvent.VK_W && mousepoint != null && st.getSelf().getrope2() == null) {
                    ClientClass.getInstance().sent(SHOOT_ROPE2, bale(Double.toString(mousepoint.getX()), Double.toString(mousepoint.getY())));
                }
                st.getSelf().keyPressed(commandCode, trigTime);
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

                if (commandCode == KeyEvent.VK_Q && mousepoint != null && st.getSelf().getrope1() != null) {
                    ClientClass.getInstance().sent(CANCEL_ROPE1, bale(""));
                }
                if (commandCode == KeyEvent.VK_W && mousepoint != null && st.getSelf().getrope2() != null) {
                    ClientClass.getInstance().sent(CANCEL_ROPE2, bale(""));
                }
                if (commandCode == KeyEvent.VK_ESCAPE) {
                    parse = !parse;
                }
                st.getSelf().keyReleased(commandCode, trigTime);
            }

            @Override
            public void keyTyped(char c, long trigTime) {
                st.getSelf().keyTyped(c, trigTime);
            }
        };
    }

    public void Force() {
        for (int i = 0; i < st.getPlayers().size(); i++) {
            if (st.getPlayers().get(i).getrope1() != null && st.getPlayers().get(i).getrope1().target() != null) {
                if (st.getPlayers().get(i).getrope1().state() == Rope.Stage.PULL) {
                    ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).getrope1().target().ID()),
                            Double.toString(st.getPlayers().get(i).getrope1().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vx() * -0.5),
                            Double.toString(st.getPlayers().get(i).getrope1().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vy() * -0.5)));
                }
            }
            if (st.getPlayers().get(i).getrope2() != null && st.getPlayers().get(i).getrope2().state() == Rope.Stage.PULL && st.getPlayers().get(i).getrope2().target() != null) {
                ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).getrope2().target().ID()),
                        Double.toString(st.getPlayers().get(i).getrope2().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vx() * -0.5),
                        Double.toString(st.getPlayers().get(i).getrope2().pull(st.getPlayers().get(i).collider().centerX(), st.getPlayers().get(i).collider().centerY()).vy() * -0.5)));
            }
        }
        st.getSelf().Force();

    }

    public void Portion() {
        st.getSelf().Portion();
        for (int j = 1; j <= MAXPOWER * COLLISION_ACCURACY; j++) {
            for (int i = 0; i < st.getBlock().size(); i++) {
                st.getSelf().collision(st.getBlock().get(i), j);
            }
            for (int i = 1; i < st.getPlayers().size(); i++) {
                Vector tmp = st.getSelf().collision(st.getPlayers().get(i), j);
                if (tmp != null) {
                    ClientClass.getInstance().sent(FORCE, bale(Integer.toString(st.getPlayers().get(i).ID()), Double.toString(tmp.vx()), Double.toString(tmp.vy())));
                }
            }
        }
    }

    public void move() {
        st.getSelf().move();
    }

}
