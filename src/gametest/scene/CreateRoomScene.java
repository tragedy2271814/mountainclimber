package gametest.scene;

import ModePackage.menumodule.menu.BackgroundType;
import ModePackage.menumodule.menu.Button;
import ModePackage.menumodule.menu.EditText;
import ModePackage.menumodule.menu.Label.ClickedAction;
import ModePackage.menumodule.menu.Style;
import ModePackage.menumodule.menu.impl.MouseTriggerImpl;
import gametest.CommandSolver;
import gametest.FontLoader;
import static gametest.Global.SCREEN_X;
import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;

public class CreateRoomScene extends Scene {
    private Image img;
    private EditText inputport;
    private EditText inputname;
    private Button create;
    private Button back;

    @Override
    public void sceneBegin() {
        img=ImageController.instance().tryGetImage(Path.Resources.Background.CONNECTSCENE_BACKGROUND);
        inputname = new EditText(SCREEN_X / 2 - 150, 300, "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        inputport = new EditText(SCREEN_X / 2 - 150, 400, "Port", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.darkGray))
                .setBorderColor(Color.lightGray)
                .setBorderThickness(3)
                .setHaveBorder(true)
                .setTextFont(FontLoader.Retro5(50f)));
        create = new Button(SCREEN_X / 2 +25, 500, new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Create")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        create.setStyleHover(new Style.StyleRect(150, 50, true,new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Create")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        create.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new ChiefScene(inputname.getEditText(),Integer.parseInt(inputport.getEditText())));
            }
        });
        back = new Button(SCREEN_X / 2 - 175, 500, new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_NORMAL)))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        back.setStyleHover(new Style.StyleRect(150, 50, true, new BackgroundType.BackgroundImage(ImageController.instance().tryGetImage(Path.Resources.Material.Label.BUTTON_HARVER)))
                .setText("Back")
                .setTextFont(FontLoader.Retro5(50f))
                .setTextColor(Color.white));
        back.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                SceneController.instance().change(new MainScene());
            }
        });
    }

    @Override
    public void sceneEnd() {
        inputname=null;
        inputport=null;
        ImageController.instance().clear();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, null);
        g.setColor(Color.white);
        g.setFont(FontLoader.Retro5(50f));
        g.drawString("Pleace enter your Name:", SCREEN_X/2-150, 280);
        g.drawString("Pleace enter PORT:", SCREEN_X/2-150, 380);
        g.setColor(Color.black);
        create.paint(g);
        back.paint(g);
        inputname.paint(g);
        inputport.paint(g);
    }

    @Override
    public void update() {
        
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(create, e, state);
                MouseTriggerImpl.mouseTrig(inputname, e, state);
                MouseTriggerImpl.mouseTrig(inputport, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {

            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

            }

            @Override
            public void keyTyped(char c, long trigTime) {
                inputport.keyTyped(c);
                inputname.keyTyped(c);
            }
        };
    }

}
