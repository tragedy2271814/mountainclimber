/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gametest;

import gametest.Global.*;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author user1
 */
public class Animator {

    private Image img;
    private final Delay delay;
    private int count;
    private State state;
    private Molding molding;
    public Animator(State state ){
        this(state,null);
    }
    public Animator(State state ,Molding molding) {
        this.molding=molding;
        img = ImageController.instance().tryGetImage(((this.molding!=null)?this.molding.path():"")+state.path());
        delay = new Delay(0);
        delay.loop();
        count = 0;
        setState(state);
    }
    public Molding molding(){
        return molding;
    }
    public State state(){
        return state;
    }
    public final void setState(State state) {
        if (this.state != state) {
            this.state = state;
            count=0;
            img = ImageController.instance().tryGetImage(((this.molding!=null)?this.molding.path():"")+state.path());
            this.delay.setLimit(state.speed());
        }
    }
    public final void setMolding(Molding molding){
        if (this.molding != molding) {
            this.molding = molding;
            img = ImageController.instance().tryGetImage(((this.molding!=null)?this.molding.path():"")+state.path());
        }
        this.molding= molding;
    }

    public void paint(int left, int top, Graphics g) {
        if (count < state.arr().length) {
            g.drawImage(img,
                    left, top,
                    left + state.width(), top + state.height(),
                    state.width() * state.arr()[count],
                    0,
                    state.width() + state.width() * state.arr()[count],
                    state.height(), null);
        }

    }

    public void update() {
        if (delay.count()) {
            if(state==State.PLAYER_ROLL_RIGHT&&count==state.arr().length-1){
                setState(State.PLAYER_WALK_RIGHT);
            }
            if(state==State.PLAYER_ROLL_LEFT&&count==state.arr().length-1){
                setState(State.PLAYER_WALK_LEFT);
            }
            count = ++count % state.arr().length;
        }
    }

}
