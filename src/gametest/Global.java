package gametest;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class Global {
    public enum Molding{
        PLAYER1(Path.Resources.Material.PLAYER1_FOLDER,0),
        PLAYER2(Path.Resources.Material.PLAYER2_FOLDER,1),
        PLAYER3(Path.Resources.Material.PLAYER3_FOLDER,2),
        PLAYER4(Path.Resources.Material.PLAYER4_FOLDER,3);
        private String path;
        private int code;
        Molding (String path,int code){
            this.path=path;
            this.code=code;
        }
        public int code(){
            return code;
        }
        public String path(){
            return path;
        }
    }
    public enum State {
        PLAYER_NORMAL_RIGHT(23, 32, new int[]{0}, 2, Path.PLAYER_NORMAL_RIGHT, 0),
        PLAYER_NORMAL_LEFT(23, 32, new int[]{0}, 2, Path.PLAYER_NORMAL_LEFT, 1),
        PLAYER_MOVE_RIGHT(33, 29, new int[]{0, 1, 2, 3}, 2, Path.PLAYER_MOVE_RIGHT, 2),
        PLAYER_MOVE_LEFT(33, 29, new int[]{3, 2, 1, 0}, 2, Path.PLAYER_MOVE_LEFT, 3),
        PLAYER_UP_RIGHT(20, 38, new int[]{0, 1, 2, 3}, 2, Path.PLAYER_UP_RIGHT, 4),
        PLAYER_UP_LEFT(20, 38, new int[]{3, 2, 1, 0}, 2, Path.PLAYER_UP_LEFT, 5),
        PLAYER_DOWN_RIGHT(24, 45, new int[]{0, 1, 2, 3}, 2, Path.PLAYER_DOWN_RIGHT, 6),
        PLAYER_DOWN_LEFT(24, 45, new int[]{3, 2, 1, 0}, 2, Path.PLAYER_DOWN_LEFT, 7),
        PLAYER_WALK_RIGHT(28, 32, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 4, Path.PLAYER_WALK_RIGHT, 8),
        PLAYER_WALK_LEFT(28, 32, new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, 4, Path.PLAYER_WALK_LEFT, 9),
        PLAYER_ROLL_RIGHT(30, 29, new int[]{ 2, 3, 4, 5, 6, 7}, 3, Path.PLAYER_ROLL_RIGHT, 10),
        PLAYER_ROLL_LEFT(30, 29, new int[]{17, 16, 15, 14, 13, 12},3, Path.PLAYER_ROLL_LEFT, 11),
        PLAYER_SHOW(22, 27, new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19},2, Path.PLAYER_SHOW, 12),
        TERMAINALPOINT_START(34, 32, new int[]{0, 1, 2, 3},3, Path.Resources.Material.TERMAINALPOINT_START, 13),
        TERMAINALPOINT_END(41, 37, new int[]{0, 1, 2, 3, 4, 5},3, Path.Resources.Material.TERMAINALPOINT_END, 14),
        TERMAINALPOINT_END2(171, 231, new int[]{0},3, Path.Resources.Material.TERMAINALPOINT_END2, 14),
        MOUNTAIN2(1280, 960, new int[]{0, 1, 2, 3, 4},5, Path.Resources.Background.MOUNTAIN2, 15),
        COVER1(1280, 960, new int[]{0, 1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56},4, Path.Resources.Cover.COVER1, 16),
        TRANSITIONS(1280, 960, new int[]{0, 1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33},6, Path.Resources.Transitions.TRANSITIONS, 17),
        TOBRIGHT(1280, 960, new int[]{0, 1, 2, 3, 4, 5,6,7,8,9,10},8, Path.Resources.Transitions.TRANSITIONS2, 18),
        TODARK(1280, 960, new int[]{10, 9, 8, 7, 6, 5,4,3,2,1,0},8, Path.Resources.Transitions.TRANSITIONS2, 19);
        
        private int width;
        private int height;
        private int[] arr;
        private int speed;
        private String path;
        private int typecode;

        State(int width, int height, int arr[], int speed, String path, int typecode) {
            this.width = width;
            this.height = height;
            this.arr = arr;
            this.speed = speed;
            this.path = path;
            this.typecode = typecode;
        }

        public int[] arr() {
            return arr;
        }

        public int speed() {
            return speed;
        }

        public int width() {
            return width;
        }

        public int height() {
            return height;
        }

        public String path() {
            return path;
        }

        public int typeCode() {
            return typecode;
        }
    }

    public enum Direction {
        LEFT,
        RIGHT;
    }
    public static final boolean IS_DEBUG = false;
    //視窗大小
    public static final int WINDOW_WIDTH = 1280;
    public static final int WINDOW_HEIGHT = 960;
    public static final int SCREEN_X = WINDOW_WIDTH - 8 - 8; //左右邊的邊界JAVA內建8
    public static final int SCREEN_Y = WINDOW_HEIGHT - 31 - 8; //上面的BAR佔28
    //地圖大小
    public static final int SINGLEMODE_MAP_WIDTH = 3200;//3200
    public static final int SINGLEMODE_MAP_HEIGHT = 12800;//12800
    public static final int CONNECTMODE_MAP_WIDTH = 3200;//3200
    public static final int CONNECTMODE_MAP_HEIGHT = 6400;//12800
    public static final int WAITINGSCENE_MAP_WIDTH = 1280 - 8 - 8;
    public static final int WAITINGSCENE_MAP_HEIGHT = 960 - 31 - 8;
    //資料刷新
    public static final int UPDATE_TIMES_PER_SEC = 60; //每秒更新60次遊戲邏輯
    public static final int NANOSECOND_PER_UPDATE = 1000000000 / UPDATE_TIMES_PER_SEC; //每一次要花費的奈秒數
    //畫面更新
    public static final int FRAME_LIMIT = 60;
    public static final int LIMIT_DELTA_TIME = 1000000000 / FRAME_LIMIT;

    //物理常數
    public static double COLLISION_THICKNESS = 20;
    public static final double PI = 3.1415926;
    public static final double FRICTION = 0.983;
    public static final double GRAVITY = 0.35;
    public static final double MAXPOWER = 35;
    public static final double COLLISION_ACCURACY=2.5;
    //角色數值

    public static final double JUMP_POWER = 40;
    public static final double JUMP_THETA = 75;
    public static final double MOVE_SPEED = 3;
    public static final double WALK_POWER = 0.7;
    //繩索數值
    public static final double ROPE_POWER = 1.3;
    public static final double ROPE_SPEED = 20;
    public static final int ROPE_COLDOWN=20;
    

    public static int random(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static double length(double x, double y) {
        return Math.sqrt((x * x) + (y * y));
    }

    public static double twoPointLength(double x, double y, double targetx, double targety) {
        return length(targetx - x, targety - y);
    }

    public static BufferedImage rotateImage(BufferedImage bufferedimage, double degree) {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();

        double newH = Math.abs(h * Math.cos(degree)) + Math.abs(w * Math.sin(degree));
        double newW = Math.abs(h * Math.sin(degree)) + Math.abs(w * Math.cos(degree));

        double deltaX = (newW - w) / 2;
        double deltaY = (newH - h) / 2;

        BufferedImage img = new BufferedImage((int) newW, (int) newH, type);
        Graphics2D graphics2d = img.createGraphics();
        graphics2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        // 以下為矩陣相乘， [translate] * [rotate] * [image的x,y]
        // 所以行為為，先rotate，再translate
        graphics2d.translate(deltaX, deltaY);
        graphics2d.rotate(degree, w / 2, h / 2);  //以中心點旋轉
        graphics2d.drawImage(bufferedimage, 0, 0, null);

        graphics2d.dispose();

        return img;
    }
}
