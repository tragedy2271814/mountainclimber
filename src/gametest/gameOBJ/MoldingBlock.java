package gametest.gameOBJ;

import gametest.Animator;
import gametest.Global.*;
import gametest.physical.Vector;
import java.awt.Graphics;

public class MoldingBlock extends Block{
    private Molding molding;
    private Animator animator;

    public MoldingBlock(int x, int y,Molding molding) {
        super(x, y, 22, 27);
        this.animator=new Animator(State.PLAYER_SHOW,molding);
        this.molding=molding;
    }
    public Molding molding(){
        return molding;
    }
    @Override
    public void paintComponent(Graphics g) {
        animator.paint((int)painter().left(),(int)painter().top(), g);
    }

    @Override
    public void update() {
        animator.update();
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x,y);
    }
}
