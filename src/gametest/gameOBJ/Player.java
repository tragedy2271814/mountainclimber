package gametest.gameOBJ;

import gametest.Animator;
import java.awt.*;
import java.awt.event.MouseEvent;
import gametest.CommandSolver;
import gametest.CommandSolver.KeyListener;
import gametest.CommandSolver.MouseCommandListener;
import gametest.Delay;
import gametest.Global.Direction;
import static gametest.Global.*;
import gametest.Path;
import gametest.camera.MapInformation;
import gametest.controller.AudioResourceController;
import gametest.gameOBJ.Rope.Stage;
import gametest.physical.Physical;
import gametest.physical.Vector;
import java.awt.event.KeyEvent;

public class Player extends GameObject implements MouseCommandListener, KeyListener, Physical.PhysicalOrder {
    private Animator animator;
    private Direction dir;
    private Physical p;
    private double mouseX;
    private double mouseY;
    private Rope rope1;
    private Rope rope2;
    private boolean isSpacePressed;
    private Vector momentum;
    private String Name;
    private int id;
    private boolean isAir;
    private boolean isSelf;
    private Delay delay;
    private int score;
    private BasicStroke bs;
    public Player(int x, int y) {
        this(x, y, "", 0);
    }

    public Player(int x, int y, String Name, int id) {
        this(x, y, Name, id, false);
    }

    public Player(int x, int y, String Name, int id, boolean isSelf) {
        super(x, y, 24, 32, x, y, 24, 32);
        this.Name = Name;
        this.id = id;
        dir = Direction.RIGHT;
        //重量 100 目前沒用到
        p = new Physical();
        //是否有接觸地面
        momentum = new Vector();
        isSpacePressed = false;
        isAir = false;
        this.isSelf = isSelf;
        this.animator = new Animator(State.PLAYER_MOVE_LEFT, Molding.PLAYER1);
        delay = new Delay(20);
        delay.loop();
        bs = new BasicStroke(3.0f);
    }
    public int getScore(){
        return this.score;
    }
    public void setScore(int score){
        this.score=score;
    }
        
    public void setID(int id) {
        this.id = id;
    }

    public Animator Animator() {
        return animator;
    }

    public void isSelf(boolean isSelf) {
        System.out.println(isSelf);
        this.isSelf = isSelf;
    }

    public int ID() {
        return id;
    }

    public String getName() {
        return Name;
    }

    //
    //釋放向動量
    public void inertia() {
        momentum = p.Consume();
        if (collider().left() + momentum.vx() < 0) {
            painter().setX(0);
            collider().setX(0);
            collider().translateY(momentum.vy());
            painter().translateY(momentum.vy());
        } else if (collider().right() + momentum.vx() > MapInformation.mapInfo().width()) {
            painter().setX(MapInformation.mapInfo().width() - painter().width() - 1);
            collider().setX(MapInformation.mapInfo().width() - collider().width() - 1);
            collider().translateY(momentum.vy());
            painter().translateY(momentum.vy());
        }
        if (collider().top() + momentum.vy() < 0) {
            painter().setY(0);
            collider().setY(0);
            painter().translateX(momentum.vx());
            collider().translateX(momentum.vx());
        } else if (collider().bottom() + momentum.vy() > MapInformation.mapInfo().height()) {
            painter().setY(MapInformation.mapInfo().height() - painter().height() - 1);
            collider().setY(MapInformation.mapInfo().height() - collider().height() - 1);
            painter().translateX(momentum.vx());
            collider().translateX(momentum.vx());
        } else {
            painter().translateX(momentum.vx());
            painter().translateY(momentum.vy());
            collider().translateX(momentum.vx());
            collider().translateY(momentum.vy());
        }
    }

    public Vector collision(GameObject go, int i) {
        momentum = p.vector();
        if (collider().bottom() + momentum.vy() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().top()
                && collider().bottom() + momentum.vy() / (MAXPOWER *COLLISION_ACCURACY ) * i < go.collider().top() + (1)
                && collider().left() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i < go.collider().right()
                && collider().right() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().left()) {
            isAir = false;
            collider().setY(go.collider().top() - collider().height() - 0.15);
            painter().setY(go.collider().top() - collider().height() - 0.15);
            Force(go.CounterForce(0, p.vector().vy()));
            return go.CounterForce(0, p.vector().vy());
        }
        if (collider().top() + momentum.vy() / (MAXPOWER * COLLISION_ACCURACY) * i < go.collider().bottom()
                && collider().top() + momentum.vy() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().bottom() - (1)
                && collider().left() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i < go.collider().right()
                && collider().right() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().left()) {
            collider().setY(go.collider().bottom() + 0.15);
            painter().setY(go.collider().bottom() + 0.15);
            Force(go.CounterForce(0, p.vector().vy()));
            return go.CounterForce(0, p.vector().vy());
        }
        if (collider().bottom() + momentum.vy() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().top()
                && collider().top() + momentum.vy() / (MAXPOWER * COLLISION_ACCURACY) * i < go.collider().bottom()
                && collider().right() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i > go.collider().left()
                && collider().right() + momentum.vx() / (MAXPOWER * COLLISION_ACCURACY) * i < go.collider().left() + (1)) {
            collider().setX(go.collider().left() - collider().width() - 0.15);
            painter().setX(go.collider().left() - collider().width() - 0.15);
            Force(go.CounterForce(p.vector().vx(), 0));
            return go.CounterForce(p.vector().vx(), 0);
        }
        if (collider().bottom() + (momentum.vy() / (MAXPOWER * COLLISION_ACCURACY)) * i > go.collider().top()
                && collider().top() + (momentum.vy() / (MAXPOWER * COLLISION_ACCURACY)) * i < go.collider().bottom()
                && collider().left() + (momentum.vx() / (MAXPOWER * COLLISION_ACCURACY)) * i < go.collider().right()
                && collider().left() + (momentum.vx() / (MAXPOWER * COLLISION_ACCURACY)) * i > go.collider().right() - (1)) {
            collider().setX(go.collider().right() + 0.15);
            painter().setX(go.collider().right() + 0.15);
            Force(go.CounterForce(p.vector().vx(), 0));
            return go.CounterForce(p.vector().vx(), 0);
        }
        return null;
    }

    public void setMouseX(double mouseX) {
        this.mouseX = mouseX;
    }

    public void setMouseY(double mouseY) {
        this.mouseY = mouseY;
    }

    public Physical physical() {
        return p;
    }

    public Rope getrope1() {
        return rope1;
    }

    public Rope getrope2() {
        return rope2;
    }

    public void setDir() {
        if (p.vector().vx() > 0) {
            dir = Direction.RIGHT;
        } else if (p.vector().vx() < 0) {
            dir = Direction.LEFT;
        }
    }
    public void reset(){
        this.isSpacePressed=false;
        this.rope1=null;
        this.rope2=null;
        momentum = new Vector();
        isSpacePressed = false;
        isAir = false;
        delay = new Delay(20);
        delay.loop();
    }
    public void setState() {
        if (Math.abs(p.vector().vx()) > Math.abs(p.vector().vy())) {
            if (Math.abs(p.vector().vx()) < 5) {
                if (dir == Direction.RIGHT) {
                    if (!isAir) {
                        if (animator.state() != State.PLAYER_ROLL_RIGHT && animator.state() != State.PLAYER_WALK_RIGHT) {
                            animator.setState(State.PLAYER_ROLL_RIGHT);
                        }
                    } else {
                        animator.setState(State.PLAYER_NORMAL_RIGHT);
                    }
                } else {
                    if (!isAir) {
                        if (animator.state() != State.PLAYER_ROLL_LEFT && animator.state() != State.PLAYER_WALK_LEFT) {
                            animator.setState(State.PLAYER_ROLL_LEFT);
                        }
                    } else {
                        animator.setState(State.PLAYER_NORMAL_LEFT);
                    }
                }
            } else if (p.vector().vx() < -0.15) {
                animator.setState(State.PLAYER_MOVE_LEFT);

            } else if (p.vector().vx() > 0.15) {
                animator.setState(State.PLAYER_MOVE_RIGHT);
            }
        } else {
            if (Math.abs(p.vector().vy()) < 2) {
                if (dir == Direction.RIGHT) {
                    animator.setState(State.PLAYER_NORMAL_RIGHT);
                } else {
                    animator.setState(State.PLAYER_NORMAL_LEFT);
                }
            } else if (p.vector().vy() > 0) {
                if (dir == Direction.RIGHT) {
                    animator.setState(State.PLAYER_DOWN_RIGHT);
                } else {
                    animator.setState(State.PLAYER_DOWN_LEFT);
                }
            } else if (p.vector().vy() < 0) {
                if (dir == Direction.RIGHT) {
                    animator.setState(State.PLAYER_UP_RIGHT);
                } else {
                    animator.setState(State.PLAYER_UP_LEFT);
                }
            }

        }
    }

    //
    //所有的自力
    //增加的力的方法必須在偏移目前力的方法前面
    //不然偏移完又新增力會出錯
    @Override
    public void Force() {
        Force(p.GRAVITY());
//        switch (dir) {
//            case UP:
//                Force(new Vector(0, -WALK_POWER));
//                break;
//            case DOWN:
//                Force(new Vector(0, WALK_POWER));
//                break;
//            case LEFT:
//                Force(new Vector(-WALK_POWER, 0));
//                break;
//            case RIGHT:
//                Force(new Vector(WALK_POWER, 0));
//                break;
//            case NOTHING:
//                break;
//            default:
//                throw new AssertionError(dir.name());
//        }
        if (rope1 != null && rope1.state() == Stage.PULL) {
            Vector dv = rope1.pull(collider().centerX(), collider().centerY());
            Force(new Vector(ROPE_POWER * dv.vx() / dv.length(), ROPE_POWER * dv.vy() / dv.length()));
        }
        if (rope2 != null && rope2.state() == Stage.PULL) {
            Vector dv = rope2.pull(collider().centerX(), collider().centerY());
            Force(new Vector(ROPE_POWER * dv.vx() / dv.length(), ROPE_POWER * dv.vy() / dv.length()));
        }
    }

    @Override
    public void Portion() {
        momentum = p.vector();
        if (rope1 != null && rope1.state() == Stage.CATCH) {
//            if (twoPointLength(collider().centerX(), collider().centerY(),
//                    rope1.collider().centerX(), rope1.collider().centerY()) > rope1.length() + 5) {
//                Force(rope1.drag());
//            }
            if (twoPointLength(collider().centerX() + momentum.vx(), collider().centerY() + momentum.vy(),
                    rope1.collider().centerX(), rope1.collider().centerY()) > rope1.length() + 7) {
                Force(rope1.tension(p.vector(), collider().centerX(), collider().centerY()));
            }

        }
        if (rope2 != null && rope2.state() == Stage.CATCH) {
//            if (twoPointLength(collider().centerX(), collider().centerY(),
//                    rope2.collider().centerX(), rope2.collider().centerY()) > rope2.length() + 5) {
//                Force(rope2.drag());
//            }
            if (twoPointLength(collider().centerX() + momentum.vx(), collider().centerY() + momentum.vy(),
                    rope2.collider().centerX(), rope2.collider().centerY()) > rope2.length() + 7) {
                Force(rope2.tension(p.vector(), collider().centerX(), collider().centerY()));
            }

        }
    }
    
    //  
    //外力接口
    public void Force(Vector v) {
        p.Force(v.vx(),v.vy());
        p.MaxVector();
    }

    //移動
    @Override
    public void move() {

        setDir();
        setState();
        inertia();
    }

    //
    //印出
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.yellow);
        g.drawString(Name, (int) painter().centerX() - 16, (int) painter().top() - 10);
        g.setColor(Color.black);
        //從角色到繩子間劃一條線當繩子
        if (rope1 != null) {
            if (rope1.state() != Stage.COLDOWN) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setStroke(bs);
                g2.setColor(Color.red);
                g2.drawLine((int) collider().centerX(), (int) collider().centerY(), (int) rope1.collider().centerX(), (int) rope1.collider().centerY());
                g2.setColor(Color.black);
                rope1.paint(g);
            }
        }
        if (rope2 != null) {
            if (rope2.state() != Stage.COLDOWN) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setStroke(bs);
                g2.setColor(Color.white);
                g2.drawLine((int) collider().centerX(), (int) collider().centerY(), (int) rope2.collider().centerX(), (int) rope2.collider().centerY());
                g2.setColor(Color.black);
                rope2.paint(g);
            }

        }
        animator.paint((int) painter().left(), (int) painter().top(), g);
    }

    public void isAir(boolean isAir) {
        this.isAir = isAir;
    }

    public void shootRope1(double mouseX, double mouseY) {
        rope1 = new Rope(collider().centerX(), collider().centerY(), mouseX, mouseY);
    }

    public void shootRope2(double mouseX, double mouseY) {
        rope2 = new Rope(collider().centerX(), collider().centerY(), mouseX, mouseY);
    }

    public void cancelRope1() {
        if(rope1!=null){
            rope1.setStage(Stage.COLDOWN);
        }
        
    }

    public void cancelRope2() {
        if(rope2!=null){
            rope2.setStage(Stage.COLDOWN);
        }
    }

    @Override
    public void update() {
        if (!touchBottom() && !isAir) {
            isAir = true;
        }
        if (isSpacePressed) {
            if (rope1 != null && rope1.state() == Stage.CATCH) {
                rope1.setStage(Stage.PULL);
            }
            if (rope2 != null && rope2.state() == Stage.CATCH) {
                rope2.setStage(Stage.PULL);
            }
        } else {
            if (rope1 != null && rope1.state() == Stage.PULL) {
                rope1.setStage(Stage.CATCH);
            }
            if (rope2 != null && rope2.state() == Stage.PULL) {
                rope2.setStage(Stage.CATCH);
            }
        }
        //最後判斷繩子長度
        if (rope1 != null) {
            rope1.updateR(collider().centerX(), collider().centerY());
            rope1.update();
            if (rope1.state() == Stage.COLDOWN) {
                if (rope1.isColdown()) {
                    rope1 = null;
                }
            }
        }
        if (rope2 != null) {
            rope2.updateR(collider().centerX(), collider().centerY());
            rope2.update();
            if (rope2.state() == Stage.COLDOWN) {
                if (rope2.isColdown()) {
                    rope2 = null;
                }
            }
        }
        animator.update();
    }

    //這邊用切換boolean的方式來判斷是否生成繩索
    //因為是不同Thread
    //所以當這邊生成或切換狀態等等 容易發生錯誤
    @Override
    public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
    }

    @Override
    public void keyPressed(int commandCode, long trigTime) {
        if (commandCode == KeyEvent.VK_SPACE) {
            isSpacePressed = true;
        }
        if (commandCode == KeyEvent.VK_Q && rope1 == null && isSelf) {
            shootRope1(mouseX, mouseY);
            AudioResourceController.getInstance().shot(Path.Resources.Sounds.ROPE);
        }
        if (commandCode == KeyEvent.VK_W && rope2 == null && isSelf) {
            shootRope2(mouseX, mouseY);
            AudioResourceController.getInstance().shot(Path.Resources.Sounds.ROPE);
        }
    }

    @Override
    public void keyReleased(int commandCode, long trigTime) {
        if (commandCode == KeyEvent.VK_Q && rope1 != null && isSelf) {
            cancelRope1();
        }
        if (commandCode == KeyEvent.VK_W && rope2 != null && isSelf) {
            cancelRope2();
        }

        if (commandCode == KeyEvent.VK_SPACE) {
            isSpacePressed = false;
        }
    }

    @Override
    public void keyTyped(char c, long trigTime) {

    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x* -1.4, y *-1.4);
    }

}
