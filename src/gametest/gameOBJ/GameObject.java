package gametest.gameOBJ;

import gametest.GameKernel.GameInterface;
import gametest.Global;
import static gametest.Global.IS_DEBUG;
import gametest.camera.MapInformation;
import gametest.physical.Physical;
import java.awt.Color;
import java.awt.Graphics;

public abstract class GameObject implements GameInterface ,Physical.Counterforce{
    
    private Rect collider;
    private Rect painter;
    
    public GameObject(double x, double y, double width, double height){
        collider = new Rect(x, y, width, height);
        painter = new Rect(x, y, width, height);
    }
    
    public GameObject(Rect rect){
        collider = rect.clone();
        painter = rect.clone();  
    }
    
    public GameObject(double x, double y, double width, double height, double x2, double y2, double width2, double height2){
        collider = new Rect(x, y, width, height);
        painter = new Rect(x2, y2, width2, height2);
        painter.setCenter(collider.centerX(), collider.centerY());
    }
    
    public GameObject(Rect rect, Rect rect2){
        collider = rect.clone();
        painter = rect2.clone();
        painter.setCenter(rect.centerX(), rect.centerY());
    }
    
    public double getX(){
        return collider.centerX();
    }
    public double getY(){
        return collider.centerY();
    }
    
    public boolean touchTop(){
        return collider.top() <= 0;
    }
    public boolean touchLeft(){
        return collider.left() <= 0;
    }
    public boolean touchRight(){
        return collider.right() >=MapInformation.mapInfo().width();
    }
    public boolean touchBottom(){
        return collider.bottom() >=MapInformation.mapInfo().height();
    }
    
    public boolean outOfScreen(){
        if(!touchLeft() || !touchRight() || !touchTop() || !touchBottom()){
            return false;
        }
        return true;
    }
    
    
    public boolean isCollision(GameObject obj){
        return collider.overlap(obj.collider);
    }
    
    public final void translate(double x, double y){
        collider.translate(x, y);
        painter.translate(x, y);
    }
    public final void translateX(double x){
        collider.translateX(x);
        painter.translateX(x);
    }
    public final void translateY(double y){
        collider.translateY(y);
        painter.translateY(y);
    }
    
    public final Rect collider(){
        return collider;
    }
    
    public final Rect painter(){
        return painter;
    }
    
    @Override
    public void paint(Graphics g) {
        paintComponent(g);
        if(IS_DEBUG){
            g.setColor(Color.red);
            collider.paint(g);
            g.setColor(Color.green);
            painter.paint(g);
            g.setColor(Color.black);
        }
    }
    
    public abstract void paintComponent(Graphics g);
    
}
