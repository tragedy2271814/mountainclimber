
package gametest.gameOBJ;

import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.physical.Vector;
import java.awt.Graphics;
import java.awt.Image;

public class IceBlock extends Block{
    public enum Type{
        BLOCK1_BOTTOM(Path.Resources.Material.Block.BLOCK1_BOTTOM),
        BLOCK1_BOTTOM1(Path.Resources.Material.Block.BLOCK1_BOTTOM1),
        BLOCK1_BOTTOM2(Path.Resources.Material.Block.BLOCK1_BOTTOM2),
        BLOCK1_LEFT(Path.Resources.Material.Block.BLOCK1_LEFT),
        BLOCK1_LEFT_BOTTOM(Path.Resources.Material.Block.BLOCK1_LEFT_BOTTOM),
        BLOCK1_LEFT_TOP(Path.Resources.Material.Block.BLOCK1_LEFT_TOP),
        BLOCK1_MID1(Path.Resources.Material.Block.BLOCK1_MID1),
        BLOCK1_MID2(Path.Resources.Material.Block.BLOCK1_MID2),
        BLOCK1_MID3(Path.Resources.Material.Block.BLOCK1_MID3),
        BLOCK1_RIGHT(Path.Resources.Material.Block.BLOCK1_RIGHT),
        BLOCK1_RIGHT_BOTTOM(Path.Resources.Material.Block.BLOCK1_RIGHT_BOTTOM),
        BLOCK1_RIGHT_TOP(Path.Resources.Material.Block.BLOCK1_RIGHT_TOP),
        BLOCK1_TOP(Path.Resources.Material.Block.BLOCK1_TOP),
        BLOCK2_LEFT_BOTTOM(Path.Resources.Material.Block.BLOCK2_LEFT_BOTTOM),
        BLOCK2_LEFT_TOP(Path.Resources.Material.Block.BLOCK2_LEFT_TOP),
        BLOCK2_RIGHT_BOTTOM(Path.Resources.Material.Block.BLOCK2_RIGHT_BOTTOM),
        BLOCK2_RIGHT_TOP(Path.Resources.Material.Block.BLOCK2_RIGHT_TOP);
        private String path;
        Type(String path){
            this.path=path;
        }
        public String Path(){
            return path;
        }
    }
    private Image img;
    private Type type;
    public IceBlock(int x, int y, int width, int height,Type type) {
        super(x, y, width, height);
        this.type=type;
        img=ImageController.instance().tryGetImage(type.Path());
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img,X(), Y(), null);
    }

    @Override
    public void update() {
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x*-1,y*-1);
    }
    
}
