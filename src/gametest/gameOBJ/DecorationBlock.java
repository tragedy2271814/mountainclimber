package gametest.gameOBJ;

import gametest.Path;
import gametest.controller.ImageController;
import gametest.controller.SceneController;
import gametest.physical.Vector;
import java.awt.Graphics;
import java.awt.Image;

public class DecorationBlock extends GameObject {
    public enum Type{
        TALL_TREE1(85,834,Path.Resources.Material.DecorationBlock.TALL_TREE1),
        TALL_TREE2(33,1024,Path.Resources.Material.DecorationBlock.TALL_TREE2),
        TALL_TREE3(82,834,Path.Resources.Material.DecorationBlock.TALL_TREE3),
        TALL_TREE4(42,995,Path.Resources.Material.DecorationBlock.TALL_TREE4),
        BRUSH1(103,69,Path.Resources.Material.DecorationBlock.BRUSH1),
        BRUSH2(111,52,Path.Resources.Material.DecorationBlock.BRUSH2),
        BRUSH3(111,56,Path.Resources.Material.DecorationBlock.BRUSH3),
        BRUSH4(111,56,Path.Resources.Material.DecorationBlock.BRUSH4),
        BRUSH5(112,54,Path.Resources.Material.DecorationBlock.BRUSH5),
        NORMAL_TREE1(170,295,Path.Resources.Material.DecorationBlock.NORMAL_TREE1),
        NORMAL_TREE2(177,298,Path.Resources.Material.DecorationBlock.NORMAL_TREE2),
        NORMAL_TREE3(177,298,Path.Resources.Material.DecorationBlock.NORMAL_TREE3),
        NORMAL_TREE4(170,295,Path.Resources.Material.DecorationBlock.NORMAL_TREE4),
        SHORT_TREE1(170,115,Path.Resources.Material.DecorationBlock.SHORT_TREE1),
        HOUSE(333,128,Path.Resources.Material.DecorationBlock.HOUSE),
        ;
            private int width;
            private int height;
            private String path;
        Type(int width,int height,String path){
            this.width=width;
            this.height=height;
            this.path=path;
        }
        public int width(){
            return width;
        }
        public int height(){
            return height;
        }
        public String path(){
            return path;
        }
    }
    private Image img;
    public DecorationBlock(double x, double y,Type type) {
        super(x, y, type.width, type.height);
        img= ImageController.instance().tryGetImage(type.path());
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img,(int)painter().left(), (int)painter().top(), null);
    }

    @Override
    public void update() {
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x,y);
    }
    
}
