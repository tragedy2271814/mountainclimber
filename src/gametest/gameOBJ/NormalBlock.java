package gametest.gameOBJ;

import gametest.Path;
import gametest.controller.ImageController;
import gametest.physical.Vector;
import java.awt.Graphics;
import java.awt.Image;

public class NormalBlock extends Block{
    public enum Type{
        NORMAL_BLOCK1_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP),
        NORMAL_BLOCK1_RIGHT(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT),
        NORMAL_BLOCK1_LEFT(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT),
        NORMAL_BLOCK1_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_BOTTOM),
        NORMAL_BLOCK1_RIGHT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_TOP),
        NORMAL_BLOCK1_LEFT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_TOP),
        NORMAL_BLOCK1_RIGHT_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM),
        NORMAL_BLOCK1_LEFT_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM),
        NORMAL_BLOCK2_LEFT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_TOP),
        NORMAL_BLOCK2_RIGHT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_TOP),
        NORMAL_BLOCK2_RIGHT_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_RIGHT_BOTTOM),
        NORMAL_BLOCK2_LEFT_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_LEFT_BOTTOM),
        NORMAL_BLOCK2_MID(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_MID),
        NORMAL_BLOCK1_ALL(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_ALL),
        NORMAL_BLOCK1_TOP_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_TOP_BOTTOM),
        NORMAL_BLOCK1_RIGHT_LEFT(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_LEFT),
        NORMAL_BLOCK2_ALL(Path.Resources.Material.NormalBlock.NORMAL_BLOCK2_ALL),
        NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_RIGHT_BOTTOM_LEFT_TOP),
        NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_LEFT_BOTTOM_RIGHT_TOP),
        NORMAL_BLOCK1_THREE_TOP(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_TOP),
        NORMAL_BLOCK1_THREE_LEFT(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_LEFT),
        NORMAL_BLOCK1_THREE_RIGHT(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_RIGHT),
        NORMAL_BLOCK1_THREE_BOTTOM(Path.Resources.Material.NormalBlock.NORMAL_BLOCK1_THREE_BOTTOM),
        ;
        private String path;
        Type(String path){
            this.path=path;
        }
        public String Path(){
            return path;
        }
    }
    private Type type;
    private Image img;
    
    public NormalBlock(int x, int y, int width, int height,Type type) {
        super(x, y, width, height);
        this.type=type;
        img=ImageController.instance().tryGetImage(type.Path());
    }
    
    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img,X(), Y(), null);
    }

    @Override
    public void update() {
        
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x*-1.2,y*-1);
    }
}
