package gametest.gameOBJ;

import gametest.Delay;
import gametest.Global;
import static gametest.Global.*;
import gametest.physical.Vector;
import java.awt.Color;
import java.awt.Graphics;

public class Rope extends GameObject {

    @Override
    public Vector CounterForce(double x, double y) {
        return new Vector(x,y);
    }

    //三種階段
    public enum Stage {
        FLY,
        CATCH,
        PULL,
        COLDOWN
        ;
    }
    private double PortionX;
    private double PortionY;
    private double dr;
    private Stage state;
    private Player target;
    private double lastx;
    private double lasty;
    private Delay delay;

    public Rope(double x, double y, double targetX, double targetY) {
        super(x, y, 5, 5, x, y, 10, 10);
        //建構子的dr只是為了算繩索移動方向
        //使用玩便初始化
        this.dr = twoPointLength(x, y, targetX, targetY);
        this.PortionX = ROPE_SPEED * (targetX - x) / dr;
        this.PortionY = ROPE_SPEED * (targetY - y) / dr;
        this.state = Stage.FLY;
        dr = 0;
        lastx = 0;
        lasty = 0;
        delay=new Delay(ROPE_COLDOWN);
        delay.play();
    }

    public Player target() {
        return target;
    }

    //
    //取長度
    public double length() {
        return dr;
    }

    //
    //提供外部切換階段的接口
    public void isPull(boolean isPull) {
        if (isPull) {
            state = Stage.PULL;
        } else {
            state = Stage.CATCH;
        }
    }

    //取得目前階段
    public Stage state() {
        return state;
    }

    public void setStage(Stage stage) {
        this.state = stage;
    }

    @Override
    public boolean isCollision(GameObject obj) {
        int d = 50;
        for (int i = 1; i <= d; i++) {
            if (collider().bottom() + PortionY / d * i > obj.collider().top()
                    && collider().top() + PortionY / d * i < obj.collider().bottom()
                    && collider().left() + PortionX / d * i < obj.collider().right()
                    && collider().right() + PortionX / d * i > obj.collider().left()) {
                if (state == Stage.FLY) {
                    collider().setCenter(collider().centerX() + PortionX / d * i, collider().centerY() + PortionY / d * i);
                    painter().setCenter(painter().centerX() + PortionX / d * i, painter().centerY() + PortionY / d * i);
                }
                return true;
            }
        }
        return false;
    }

    public void collision(Player player) {
        int d = 50;
        for (int i = 1; i <= d; i++) {
            if (collider().bottom() + PortionY / d * i > player.collider().top()
                    && collider().top() + PortionY / d * i < player.collider().bottom()
                    && collider().left() + PortionX / d * i < player.collider().right()
                    && collider().right() + PortionX / d * i > player.collider().left()) {
                this.target = player;
                collider().setCenter(target.collider().centerX(), target.collider().centerY());
                painter().setCenter(target.collider().centerX(), target.collider().centerY());
                state = Stage.CATCH;
            }
        }
    }

    //往中心點拉取等效的力
    //需要彈力繩的時候可以使用
    //目前還用不到
    public Vector drag(Vector v, double x, double y) {
        double dx = collider().centerX() - x;
        double dy = collider().centerY() - y;
        double dr = Global.length(dx, dy);
        if (dr >= this.dr) {
            return new Vector(v.length() * dx / dr, v.length() * dy / dr);
        }
        return v;
    }

    //
    //收繩索
    //並回傳一個往中心的力
    public Vector pull(double x, double y) {
        double dx = collider().centerX() - x;
        double dy = collider().centerY() - y;
        return new Vector(ROPE_POWER * dx / dr,
                ROPE_POWER * dy / dr);
    }

    //
    //繩索張力
    //回傳一個可以抵銷離心力的向心力
    //這邊做了一些例外處理
    //使用三角函數有幾種狀況會傳出NaN
    //必須處理掉
    public Vector tension(Vector v, double x, double y) {
        if (v.length() <= 0) {
            return new Vector(0, 0);
        }
        double dx = collider().centerX() - x;
        double dy = collider().centerY() - y;
        Vector tmpV = new Vector(dx, dy);
        double theta;
        if (dx != 0 || dy != 0) {
            theta = Vector.radianBetween(tmpV, v);
        } else {
            theta = v.theta();
        }
        double tmpLength = v.length() * Math.cos(theta);
        return new Vector(-tmpLength * 1 * dx / dr, -tmpLength * 1 * dy / dr);
    }

    public boolean isColdown(){
        return (delay.count());
    }
    //傳入一個座標可以更新目前繩索的長度
    public void updateR(double x, double y) {
        if(state==Stage.FLY){
            translate(PortionX, PortionY);
            dr=twoPointLength( x, y,collider().centerX(), collider().centerY())+Global.length(PortionX, PortionY);
        }
        if (state == Stage.PULL) {
            double dx = collider().centerX() - x;
            double dy = collider().centerY() - y;
            dr = Global.length(dx, dy);
        }
    }

    //更新階段
    @Override
    public void update() {
        lastx = collider().centerX();
        lasty = collider().centerY();
        if (state != Stage.FLY && target != null) {
            collider().setCenter(target.collider().centerX(), target.collider().centerY());
            painter().setCenter(target.collider().centerX(), target.collider().centerY());
        }

    }

    public Vector drag() {
        return new Vector(collider().centerX() - lastx, collider().centerY() - lasty);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect((int) collider().left(), (int) collider().top(), (int) collider().width(), (int) collider().height());
        g.setColor(Color.black);
    }
}
