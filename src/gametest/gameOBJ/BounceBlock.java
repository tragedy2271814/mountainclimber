/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gametest.gameOBJ;

import gametest.Path;
import gametest.controller.ImageController;
import gametest.physical.Vector;
import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author user
 */
public class BounceBlock extends Block{
    private Image img;
    public BounceBlock(int x, int y, int width, int height) {
        super(x, y, width, height);
        img=ImageController.instance().tryGetImage(Path.Resources.Material.BOUNCEBLOCK1);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, X(), Y(), null);
    }

    @Override
    public void update() {
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x*-2.7,y*-2.7);
    }
    
}
