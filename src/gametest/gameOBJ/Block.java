package gametest.gameOBJ;

import gametest.physical.Physical;

public abstract class Block extends GameObject implements Physical.Counterforce {
    
    private int x;
    private int y;
    private int width;
    private int height;
    
    public Block(int x, int y, int width, int height) {
        super(x, y, width, height , x, y, width, height);
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }
    public int X(){
        return x;
    }
    public int Y(){
        return y;
    }
    public int Width(){
        return width;
    }
    public int Height(){
        return height;
    }
}
