package gametest.gameOBJ;

import gametest.Animator;
import gametest.Global.*;
import gametest.physical.Vector;
import java.awt.Graphics;

public class TerminalPoint extends GameObject{
    private Animator animator;
    public TerminalPoint(double x, double y,State state) {
        super(x, y, state.width(),state.height());
        this.animator=new Animator(state);
    }
    public void setState(State state){
        this.animator.setState(state);
    }
    public Animator animator(){
        return animator;
    }
    @Override
    public void paintComponent(Graphics g) {
        animator.paint((int)painter().left(), (int)painter().top(), g);
    }

    @Override
    public void update() {
        animator.update();
    }

    @Override
    public Vector CounterForce(double x,double y) {
        return new Vector(x,y);
    }
    
}
