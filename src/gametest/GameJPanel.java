package gametest;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.*;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GameJPanel extends JPanel{
    
    public class MyListener extends MouseAdapter{
        @Override
        public void mouseMoved(MouseEvent e){
            changeDir(e.getX());
        }
}
    
    private Image img;
    private int x;
    private int y;
    private int dir;
    
    public GameJPanel(){
        this.x = 30;
        this.y = 250;
        this.dir = 0;
        
        try {
            img = ImageIO.read(getClass().getResource("/resources/airplane1.png"));
        } catch (IOException ex) {
            Logger.getLogger(GameJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.addMouseListener(new MyListener());
        this.addMouseMotionListener(new MyListener());
    }
    
    @Override
    public void paintComponent(Graphics g){
        g.drawImage(img, x, y, this);
    }
    
    public void move(){
        if(dir == 0){
            x += 4;
        }else{
            x -= 4;
        }
    }
    
    public void changeDir(int x){
        if(this.x > x){
            dir = 1;
        }else{
            dir = 0;
        }
    }
}
